package cn.etuo.ahccflow.smgp;

import cn.etuo.ahccflow.module.customer.CustomerService;
import com.wondertek.ctmp.protocol.smgp.SMGPDeliverListener;
import com.wondertek.ctmp.protocol.smgp.SMGPDeliverMessage;
import com.ytsms.platform.impl.SMGPEngine;
import com.ytsms.platform.impl.SubmitRespMessage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;

public class SMGPCaller implements InitializingBean, DisposableBean {

    private static final Logger logger = LoggerFactory.getLogger(SMGPCaller.class);
    private SMGPEngine engine;
    @Autowired
    private CustomerService service;

    @Override
    public void destroy() {
	if (engine != null) {
	    engine.disconnect();
	    logger.info("关闭短信网关监听");
	}
    }

    public SubmitRespMessage submit(String content, String mobile, String adrr) throws IOException {
	if (!isSmsEnabled) {
	    logger.debug("env.sms=false 模式下不能发短信");
	    SubmitRespMessage r = new SubmitRespMessage();
	    r.Desc = "系统当前处于离线模式，不能发送短信！这是一条测试性应答。";
	    r.Status = 0;
	    return r;
	}
	return getEngine().submit(content, mobile, adrr);
    }
    @Value("#{settings['smgp.host']}")
    private String host;
    @Value("#{settings['smgp.port']}")
    private String port;
    @Value("#{settings['smgp.user']}")
    private String user;
    @Value("#{settings['smgp.pass']}")
    private String pass;
    @Value("#{settings['smgp.spNumber']}")
    private String spNumber;
    @Value("#{settings['env.sms']}")
    private boolean isSmsEnabled;

    @Override
    public void afterPropertiesSet() throws Exception {
	if (!isSmsEnabled) {
	    logger.debug("env.sms=false 模式下不监听短信网关");
	    return;
	}
	final Map<String, String> confParameters = new HashMap<String, String>();
	confParameters.put("smgp.host", host);
	confParameters.put("smgp.port", port);
	confParameters.put("smgp.user", user);
	confParameters.put("smgp.pass", pass);
	confParameters.put("smgp.spNumber", spNumber);
	Thread startConnection=new Thread(new Runnable() {

	    @Override
	    public void run() {
		connectToSmgp(confParameters);
	    }
	});
	startConnection.setDaemon(true);
	startConnection.start();
    }

    private void connectToSmgp(Map<String, String> confParameters) {
	logger.info("初始化连接：{}", confParameters);
	// 初始化连接
	SMGPEngine smgpEngine = new SMGPEngine();
	smgpEngine.init(confParameters);
	logger.info("开始监听短信网关...");
	smgpEngine.addHanlder(new SMGPDeliverListener() {
	    @Override
	    public void onDeliver(SMGPDeliverMessage msg) {
		try {
		    if (msg.getIsReport() == 0) {
			logger.info("接收到上行短信...");
			String sim = msg.getSrcTermId();
			String content = new String(msg.getBMsgContent());
			logger.info("上行短信内容 : {} 号码 : {}", content, sim);
			if (!StringUtils.hasText(sim) || sim.length() != 11) {
			    logger.info("不支持的手机号码：{}", sim);
			    return;
			}
			if (!content.startsWith("$$sms_reg%%+")) {
			    logger.debug("不识别的短信");
			    return;
			}
			String uuid = content.substring(12);
			if (uuid.length() != 36) {
			    logger.info("不支持的绑定key：{}", uuid);
			    return;
			}
			service.bind(uuid, sim);
		    }
		} catch (Exception ex) {
		    logger.error("短信注册失败", ex);
		}
	    }
	});
	engine = smgpEngine;
    }

    public SMGPEngine getEngine() {
	return engine;
    }
}
