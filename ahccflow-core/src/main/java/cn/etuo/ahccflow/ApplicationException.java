/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.etuo.ahccflow;

public class ApplicationException extends Exception {

    public static final int UNKNOWN = -1;
    public static final int NETWORK = -2;
    public static final int UNREGIST = -9;
    public static final int RESOURCE_MISSION = -8;
    public static final int ILLEGAL_ARGS = -7;
    
    public static final int REPLICATION_ORDER=-22;
    //--------------------------------------------
    private int errorCode = UNKNOWN;

    public ApplicationException() {
    }

    public ApplicationException(String message) {
        super(message);
    }

    public ApplicationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ApplicationException(Throwable cause) {
        super(cause);
    }

    public ApplicationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public int getErrorCode() {
        return errorCode;
    }

    protected void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }
}
