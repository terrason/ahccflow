/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.etuo.ahccflow;

/**
 * 返回值.
 *
 * @author terrason
 */
public class BaseResponse {

    /**
     * 操作结果.
     * <ul><li>0-操作成功</li>
     * <li>大于0-操作成功但带有警告</li>
     * <li>小于0-操作失败</li></ul>
     */
    private int code;
    /**
     * 操作结果描述.
     */
    private String description;
    /**
     * 操作返回的数据. 可能为{@code null}.
     */
    private Object data;

    /**
     * 操作结果.
     * <ul><li>0-操作成功</li>
     * <li>大于0-操作成功但带有警告</li>
     * <li>小于0-操作失败</li></ul>
     *
     * @return the code
     */
    public int getCode() {
        return code;
    }

    /**
     * 操作结果.
     * <ul><li>0-操作成功</li>
     * <li>大于0-操作成功但带有警告</li>
     * <li>小于0-操作失败</li></ul>
     *
     * @param code the code to set
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * 操作结果描述.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 操作结果描述.
     *
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 操作返回的数据. 可能为{@code null}.
     *
     * @return the data
     */
    public Object getData() {
        return data;
    }

    /**
     * 操作返回的数据. 可能为{@code null}.
     *
     * @param data the data to set
     */
    public void setData(Object data) {
        this.data = data;
    }
}
