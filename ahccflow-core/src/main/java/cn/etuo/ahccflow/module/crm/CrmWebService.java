/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.etuo.ahccflow.module.crm;

import cn.etuo.ahccflow.module.customer.Customer;
import cn.etuo.ahccflow.module.crm.axis2.CrmWebServiceStub;
import cn.etuo.ahccflow.module.crm.axis2.CrmWebServiceStub.CallService;
import cn.etuo.ahccflow.module.crm.axis2.CrmWebServiceStub.CallServiceResponse;
import cn.etuo.ahccflow.module.crm.xmltemplate.FreemarkerConfiguration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.io.StringWriter;
import java.rmi.RemoteException;
import java.util.List;
import org.apache.axis2.AxisFault;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * 调用CRM接口的服务.
 *
 * @author terrason
 */
@Service
public class CrmWebService {

    private static final Logger logger = LoggerFactory.getLogger(CrmWebService.class);
    @Autowired
    private FreemarkerConfiguration freemarker;
    @Value("#{settings['sys.crm.webservice.url']}")
    private String url;

    @Value("#{settings['env.crm']}")
    private boolean enabled;
    /**
     * DO NOT use this field directly,use {@link #getWsstub() } instead !
     */
    private CrmWebServiceStub _wsstub;

    /**
     * 调用业务办理接口.
     */
    public void order(OrderRequest request) throws RemoteException, CrmRespondFailureException {
	logger.info("调用CRM接口订购流量包");
	if (!enabled) {
	    logger.info("evn.crm=false 模式下系统不调用crm接口！");
	    return;
	}

	try {
	    Template template = freemarker.getOrderTemplate();
	    callCrmByTemplate("办理业务", "OfferAccept", template, request);
	} catch (RemoteException ex) {
	    throw ex;
	} catch (IOException ex) {
	    throw new RuntimeException("读取模板文件出错", ex);
	} catch (TemplateException ex) {
	    throw new RuntimeException("模板操作出错", ex);
	}
    }

    /**
     * 调用登录接口.
     */
    public Customer login(QueryCustomerRequest request) throws RemoteException, CrmRespondFailureException {
	logger.info("调用CRM接口认证用户");
	if (!enabled) {
	    logger.info("evn.crm=false 模式下系统不调用crm接口，返回测试值！");
	    return createTestCustomer(request.getMobile(), request.getHcode());
	}
	try {
	    Template template = freemarker.getLoginTemplate();
	    Document responseDocument = callCrmByTemplate("用户登录", "Login", template, request);
	    Node loginNode = responseDocument.selectSingleNode("/ESB/contractBody/Login");
	    Customer customer = new Customer();
	    customer.setAddress(loginNode.selectSingleNode("custAddress").getText());
	    customer.setCertCode(loginNode.selectSingleNode("certCode").getText());
	    customer.setCertName(loginNode.selectSingleNode("certName").getText());
	    customer.setCertType(loginNode.selectSingleNode("certType").getText());
	    customer.setContact(loginNode.selectSingleNode("contactNumber").getText());
	    customer.setCustId(loginNode.selectSingleNode("custId").getText());
	    customer.setName(loginNode.selectSingleNode("cusName").getText());
	    return customer;
	} catch (RemoteException ex) {
	    throw ex;
	} catch (IOException ex) {
	    throw new RuntimeException("读取模板文件出错", ex);
	} catch (TemplateException ex) {
	    throw new RuntimeException("模板操作出错", ex);
	}
    }

    public Customer queryRelation(QueryCustomerRequest request) throws RemoteException, CrmRespondFailureException {
	logger.info("调用CRM接口获取用户销售品关系");
	if (!enabled) {
	    logger.info("evn.crm=false 模式下系统不调用crm接口，返回测试值！");
	    return createTestCustomer(request.getMobile(), request.getHcode());
	}
	try {
	    Template template = freemarker.getQueryRelationTemplate();
	    Document responseDocument = callCrmByTemplate("查询客户销售品关系", "QueryCustOfr", template, request);

	    Customer customer = new Customer();
	    List<Node> offerNodes = responseDocument.selectNodes("/ESB/contractBody/QueryCustOfr/orderedOfferingSets/orderedOfferingSet");
	    for (Node offerNode : offerNodes) {
		List<Node> productNodes = offerNode.selectNodes("prodSets/prodSet");
		for (Node productNode : productNodes) {
		    Node mobileNode = productNode.selectSingleNode("serviceNbr");
		    if (mobileNode != null && request.getMobile().equals(mobileNode.getText())) {
			logger.debug("确定订购关系");
			customer.setProductId(productNode.selectSingleNode("productId").getText());
			customer.setPrepaid("1".equals(productNode.selectSingleNode("isPrepay").getText()));
			logger.debug("mobile[{}]<-->productId[{}]<-->prepaid[{}]", request.getMobile(), customer.getProductId(), customer.isPrepaid());
			break;
		    }
		}
		if (customer.hasRelation()) {
		    break;
		}
	    }
	    return customer;
	} catch (RemoteException ex) {
	    throw ex;
	} catch (IOException ex) {
	    throw new RuntimeException("读取模板文件出错", ex);
	} catch (TemplateException ex) {
	    throw new RuntimeException("模板操作出错", ex);
	}
    }

    private Document callCrmByTemplate(String title, String code, Template template, Object request) throws IOException, RemoteException, TemplateException, CrmRespondFailureException {
	//从模板加载XML
	StringWriter xmlBuffer = new StringWriter();
	logger.debug("输入参数：{}", request);
	template.process(request, xmlBuffer);
	String xml = xmlBuffer.toString();

	//调用接口
	CallService param = new CallService();
	logger.debug("{}：{}", title, xml);
	param.setXml(xml);
	CallServiceResponse callServiceResponse = getWsstub().callService(param);
	String response = callServiceResponse.getOut();
	logger.debug("返回值：{}", response);

	//解析返回值XMl
	Document responseDocument;
	try {
	    responseDocument = DocumentHelper.parseText(response);
	} catch (DocumentException ex) {
	    throw new CrmResponseUnparsableException(ex.getMessage(), ex);
	}
	Node resultNode = responseDocument.selectSingleNode("/ESB/contractBody/" + code + "/result");
	if (resultNode == null) {
	    throw new CrmResponseUnparsableException("xml文档中不存在节点：" + "/ESB/contractBody/" + code + "/result");
	}
	if (!"0".equals(resultNode.getText())) {
	    Node mesNode = responseDocument.selectSingleNode("/ESB/contractBody/" + code + "/msg");
	    throw new CrmRespondFailureException(mesNode == null ? null : mesNode.getText());
	}
	return responseDocument;
    }

    private CrmWebServiceStub getWsstub() throws AxisFault {
	if (_wsstub == null) {
	    _wsstub = new CrmWebServiceStub(url);
	}
	return _wsstub;
    }

    private Customer createTestCustomer(String mobile, int hcode) {
	Customer customer = new Customer();
	customer.setMobile(mobile);
	customer.setHcode(hcode);
	customer.setAddress("安徽省合肥市蜀山开发区湖光路电子产业园");
	customer.setCertCode("340823198902010544");
	customer.setCertName("身份证");
	customer.setCertType("1001");
	customer.setContact("18056080891");
	customer.setCustId("55103945889");
	customer.setKey("460036950096944");
	customer.setName("iio");
	customer.setPrepaid(false);
	customer.setProductId("200672214");
	return customer;
    }
}
