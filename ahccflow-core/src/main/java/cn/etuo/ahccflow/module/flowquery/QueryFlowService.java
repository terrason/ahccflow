/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.etuo.ahccflow.module.flowquery;

import cn.etuo.ahccflow.ApplicationException;
import cn.etuo.ahccflow.module.customer.Customer;
import cn.etuo.ahccflow.ctmp.qryflowservice.IFQryFlowDto;
import cn.etuo.ahccflow.ctmp.qryflowservice.QryFlowUtil;
import cn.etuo.ahccflow.module.customer.CustomerService;
import cn.etuo.ahccflow.module.udb.QueryMobileService;
import cn.etuo.ahccflow.module.udb.UdbRespondFailureException;
import java.io.IOException;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.terramagnet.common.asyndb.AsyncDao;

@Service
public class QueryFlowService {

    private final Logger logger = LoggerFactory.getLogger(QueryFlowService.class);
    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
    @Value("#{settings['sys.flow.query.url']}")
    private String url;
    @Value("#{settings['env.flow']}")
    private boolean isFlowqueryEnabled;
    @Resource
    private CustomerService customerService;
    @Autowired
    private QueryMobileService queryMobileService;
    @Resource(name = "springJdbcAsyncDao")
    private AsyncDao asyncDao;

    public IFQryFlowDto queryFlow(String key) throws IOException, ApplicationException {
        if (!isFlowqueryEnabled) {
            logger.info("evn.flow=false 模式下系统不调用查询流量接口，返回默认值！");
            return getTestFlow();
        }
        logger.info("用户（key：{}）查询流量",key);
        Customer entity = customerService.findCustomerByKey(key);
        if(entity==null){
            String mobile = queryMobileService.queryMobileBySim(key);
            Customer customer = customerService.bind(key, mobile);
            customerService.authenticate(customer);
            customerService.linkRelation(customer);
            entity=customer;
        }
        String currentMonth = getFormattedCurrentMonth();
        String hcode = String.valueOf(entity.getHcode());
        QryFlowUtil util = new QryFlowUtil();
        util.setUrl(url);

        logger.debug("{} {}@{}-{}", url, entity.getMobile(), currentMonth, hcode);
        return util.queryFlow(entity.getMobile(), currentMonth, hcode);
    }

    private String getFormattedCurrentMonth() {
        return sdf.format(new Date());
    }

    public void saveHistory(String key, IFQryFlowDto dto) {
        asyncDao.update("insert into history_queryflow(customer_id,channel,create_time,total,used,`leave`,over,status,description) select id,1,now(),?,?,?,?,?,? from customer where sim_key=?", dto.getTotal(), dto.getUsed(), dto.getLeave(), dto.getOver(), dto.getCode(), dto.getDesc(), key);
    }

    private IFQryFlowDto getTestFlow() {
        IFQryFlowDto dto = new IFQryFlowDto();
        dto.setCode(0);
        dto.setDesc("这是一个静态的测试流量");
        dto.setLeave(10);
        dto.setOver(0);
        dto.setTotal(40);
        dto.setUsed(30);
        return dto;
    }
}
