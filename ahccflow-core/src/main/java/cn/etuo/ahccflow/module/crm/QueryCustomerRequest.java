/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.etuo.ahccflow.module.crm;

public class QueryCustomerRequest {

    public QueryCustomerRequest() {
    }

    public QueryCustomerRequest(String mobile, int hcode) {
        this.mobile = mobile;
        this.hcode = hcode;
    }
    private String mobile;
    private int hcode;
    private final String sysmillis = Long.toString(System.currentTimeMillis());

    public String getSysmillis() {
        return sysmillis;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getHcode() {
        return hcode;
    }

    public void setHcode(int hcode) {
        this.hcode = hcode;
    }

    @Override
    public String toString() {
        return "QueryCustomerRequest{" + "mobile=" + mobile + ", hcode=" + hcode + '}';
    }
}
