/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.etuo.ahccflow.module.crm;

import cn.etuo.ahccflow.ApplicationException;
/**
 * 当CRM接口返回错误提示时抛出此异常.
 * 异常代码：-22
 * @author terrason
 */
public class CrmRespondFailureException extends ApplicationException {

    public static final int ERROR_CODE = -22;

    public CrmRespondFailureException(String message) {
        super("CRM接口返回错误提示：" + message);
        setErrorCode(ERROR_CODE);
    }
}
