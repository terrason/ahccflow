/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.etuo.ahccflow.module.flowpack;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * 流量包.
 *
 * @author terrason
 */
public class Flowpack {

    /**
     * 普通流量包.
     */
    public static final int CATALOG_NORMAL = 0;
    /**
     * 加餐包.
     */
    public static final int CATALOG_ADDITIONAL = 1;

    private int id;
    private String name;
    private String mobile;
    @JsonIgnore
    private String icon;
    private boolean prepaid;
    private double price;
    @JsonIgnore
    private String offerId;
    @JsonIgnore
    private String groupId;
    @JsonIgnore
    private int expiration;
    @JsonIgnore
    private int effection;
    private boolean enabled;
    private boolean hot;

    private int catalog;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public boolean isPrepaid() {
        return prepaid;
    }

    public void setPrepaid(boolean prepaid) {
        this.prepaid = prepaid;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getMobile() {
        if (mobile == null) {
            return null;
        }
        if (mobile.length() < 11) {
            return "";
        } else {
            return mobile.substring(0, 3) + "*****" + mobile.substring(8);
        }
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getCatalog() {
        return catalog;
    }

    public void setCatalog(int catalog) {
        this.catalog = catalog;
    }

    public int getExpiration() {
        return expiration;
    }

    public void setExpiration(int expiration) {
        this.expiration = expiration;
    }

    public int getEffection() {
        return effection;
    }

    public void setEffection(int effection) {
        this.effection = effection;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isHot() {
        return hot;
    }

    public void setHot(boolean hot) {
        this.hot = hot;
    }

    @Override
    public String toString() {
        return "Flowpack{" + "id=" + id + ", name=" + name + ", mobile=" + mobile + ", icon=" + icon + ", prepaid=" + prepaid + ", price=" + price + ", offerId=" + offerId + ", groupId=" + groupId + ", catalog=" + catalog + '}';
    }

}
