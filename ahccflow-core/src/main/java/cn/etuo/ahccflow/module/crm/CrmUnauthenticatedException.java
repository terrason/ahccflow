/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.etuo.ahccflow.module.crm;

import cn.etuo.ahccflow.ApplicationException;
import cn.etuo.ahccflow.module.customer.Customer;
/**
 * 客户无法完成CRM登录时抛出此异常.
 * 异常代码：-23
 * @author terrason
 */
public class CrmUnauthenticatedException extends ApplicationException {
    public static final int ERROR_CODE = -23;
    public CrmUnauthenticatedException(Customer customer) {
        super("crm登录失败：" + customer);
        setErrorCode(ERROR_CODE);
    }

    public CrmUnauthenticatedException(Customer customer, Throwable cause) {
        super("crm登录失败：" + customer, cause);
    }
}
