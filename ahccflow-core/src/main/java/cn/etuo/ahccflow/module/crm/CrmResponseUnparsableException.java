/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package cn.etuo.ahccflow.module.crm;

/**
 * 解析CRM返回值内容出错.
 * @author terrason
 */
public class CrmResponseUnparsableException extends RuntimeException{

    public CrmResponseUnparsableException() {
    }

    public CrmResponseUnparsableException(String message) {
        super(message);
    }

    public CrmResponseUnparsableException(String message, Throwable cause) {
        super(message, cause);
    }

    public CrmResponseUnparsableException(Throwable cause) {
        super(cause);
    }

}
