/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.etuo.ahccflow.module.crm;

import cn.etuo.ahccflow.module.customer.Customer;
import cn.etuo.ahccflow.module.flowpack.Flowpack;

public class OrderRequest {

    public OrderRequest() {
    }

    public OrderRequest(Customer customer, Flowpack flow) {
        this.customer = customer;
        this.flow = flow;
    }
    private Customer customer;
    private Flowpack flow;
    private String sysmillis = Long.toString(System.currentTimeMillis());

    public String getSysmillis() {
        return sysmillis;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Flowpack getFlow() {
        return flow;
    }

    public void setFlow(Flowpack flow) {
        this.flow = flow;
    }

    @Override
    public String toString() {
        return "OrderRequest{" + "customer=" + customer + ", flow=" + flow + ", sysmillis=" + sysmillis + '}';
    }
}
