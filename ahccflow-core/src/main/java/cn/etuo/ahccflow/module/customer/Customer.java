/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.etuo.ahccflow.module.customer;

import org.springframework.util.StringUtils;

public class Customer {

    private int id;
    /**
     * uuid.
     */
    private String key;
    /**
     * 手机号.
     */
    private String mobile;
    /**
     * 地域编码.
     */
    private int hcode;
    /**
     * 客户编号.
     */
    private String custId;
    /**
     * 客户名称.
     */
    private String name;
    /**
     * 客户对应产品编号.
     */
    private String productId;
    /**
     * 预付费的.
     */
    private boolean prepaid;
    /**
     * 客户地址.
     */
    private String address;
    /**
     * 联系方式.
     */
    private String contact;
    /**
     * 证件类型编码（见附录编码）.
     */
    private String certType;
    /**
     * 证件类型名称.
     */
    private String certName;
    /**
     * 证件号码.
     */
    private String certCode;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * uuid.
     *
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * uuid.
     *
     * @param key the key to set
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * 手机号.
     *
     * @return the mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 手机号.
     *
     * @param mobile the mobile to set
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 地域编码.
     *
     * @return the hcode
     */
    public int getHcode() {
        return hcode;
    }

    /**
     * 地域编码.
     *
     * @param hcode the hcode to set
     */
    public void setHcode(int hcode) {
        this.hcode = hcode;
    }

    /**
     * 客户编号.
     *
     * @return the custId
     */
    public String getCustId() {
        return custId;
    }

    /**
     * 客户编号.
     *
     * @param custId the custId to set
     */
    public void setCustId(String custId) {
        this.custId = custId;
    }

    /**
     * 客户名称.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * 客户名称.
     *
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 客户对应产品编号.
     *
     * @return the productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     * 客户对应产品编号.
     *
     * @param productId the productId to set
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     * 预付费的.
     *
     * @return the prepaid
     */
    public boolean isPrepaid() {
        return prepaid;
    }

    /**
     * 预付费的.
     *
     * @param prepaid the prepaid to set
     */
    public void setPrepaid(boolean prepaid) {
        this.prepaid = prepaid;
    }

    /**
     * 客户地址.
     *
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * 客户地址.
     *
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 联系方式.
     *
     * @return the contact
     */
    public String getContact() {
        return contact;
    }

    /**
     * 联系方式.
     *
     * @param contact the contact to set
     */
    public void setContact(String contact) {
        this.contact = contact;
    }

    /**
     * 证件类型编码（见附录编码）.
     *
     * @return the certType
     */
    public String getCertType() {
        return certType;
    }

    /**
     * 证件类型编码（见附录编码）.
     *
     * @param certType the certType to set
     */
    public void setCertType(String certType) {
        this.certType = certType;
    }

    /**
     * 证件类型名称.
     *
     * @return the certName
     */
    public String getCertName() {
        return certName;
    }

    /**
     * 证件类型名称.
     *
     * @param certName the certName to set
     */
    public void setCertName(String certName) {
        this.certName = certName;
    }

    /**
     * 证件号码.
     *
     * @return the certCode
     */
    public String getCertCode() {
        return certCode;
    }

    /**
     * 证件号码.
     *
     * @param certCode the certCode to set
     */
    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    /**
     * 是否是已登录用户.
     */
    public boolean isAuthenticated() {
        return StringUtils.hasText(getCustId());
    }

    /**
     * 是否含有销售品关系.
     */
    public boolean hasRelation() {
        return StringUtils.hasText(getProductId());
    }

    @Override
    public String toString() {
        return "Customer{" + "id=" + getId() + ", mobile=" + getMobile() + ", hcode=" + getHcode() + ", custId=" + getCustId() + ", name=" + getName() + ", productId=" + getProductId() + ", prepaid=" + isPrepaid() + ", address=" + getAddress() + ", contact=" + getContact() + ", certType=" + getCertType() + ", certName=" + getCertName() + ", certCode=" + getCertCode() + '}';
    }
}
