/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.etuo.ahccflow.module.customer;

import cn.etuo.ahccflow.ApplicationException;

public class ReplicateBindingException extends ApplicationException {

    public static final int ERROR_CODE = -25;

    public ReplicateBindingException(String key) {
        super("重复的绑定key：" + key);
        setErrorCode(ERROR_CODE);
    }

    public ReplicateBindingException(String key, Throwable cause) {
        super("重复的绑定key：" + key, cause);
    }
}
