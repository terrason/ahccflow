/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.etuo.ahccflow.module.crm;

import cn.etuo.ahccflow.ApplicationException;
import cn.etuo.ahccflow.module.customer.Customer;

/**
 * 客户无法验证CRM订购关系时抛出此异常. 异常代码：-24
 *
 * @author terrason
 */
public class CrmUnknownRelationException extends ApplicationException {

    public static final int ERROR_CODE = -24;

    public CrmUnknownRelationException(Customer customer) {
        super("未知的订购关系：" + customer);
        setErrorCode(ERROR_CODE);
    }

    public CrmUnknownRelationException(Customer customer, Throwable cause) {
        super("未知的订购关系：" + customer, cause);
    }
}
