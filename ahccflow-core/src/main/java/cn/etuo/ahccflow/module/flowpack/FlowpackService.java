/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.etuo.ahccflow.module.flowpack;

import cn.etuo.ahccflow.ApplicationException;
import cn.etuo.ahccflow.BaseResponse;
import cn.etuo.ahccflow.module.crm.CrmUnauthenticatedException;
import cn.etuo.ahccflow.module.crm.CrmUnknownRelationException;
import cn.etuo.ahccflow.module.crm.CrmWebService;
import cn.etuo.ahccflow.module.crm.OrderRequest;
import cn.etuo.ahccflow.module.customer.CustomerService;
import cn.etuo.ahccflow.module.customer.Customer;
import java.rmi.RemoteException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.terramagnet.common.asyndb.AsyncDao;

/**
 * 流量包服务.
 *
 * @author terrason
 */
@Service
public class FlowpackService {

    private final Logger logger = LoggerFactory.getLogger(FlowpackService.class);

    @Autowired
    private CrmWebService crm;
    @Autowired
    private CustomerService customers;
    @Resource
    private JdbcTemplate jdbcTemplate;

    @Resource(name = "springJdbcAsyncDao")
    private AsyncDao asyncDao;

    /**
     * 分页获取流量包信息.
     *
     * @param start 分页数据起始位置
     * @param length 分页获取的数据量
     * @return 流量包列表
     */
    public BaseResponse pageFlowpacks(int start, int length){
	BaseResponse response = new BaseResponse();

	List<Flowpack> flowpacks = jdbcTemplate.query("select f.id,f.name,f.icon,f.price,f.catalog,f.hot from flowpack f order by f.hot desc,f.priority limit ?,?", new RowMapper<Flowpack>() {
	    @Override
	    public Flowpack mapRow(ResultSet rs, int rowNum) throws SQLException {
		Flowpack flowpack = new Flowpack();
		flowpack.setId(rs.getInt("id"));
		flowpack.setName(rs.getString("name"));
		flowpack.setIcon(rs.getString("icon"));
		flowpack.setPrice(rs.getDouble("price"));
		flowpack.setCatalog(rs.getInt("catalog"));
		flowpack.setHot(rs.getBoolean("hot"));
		flowpack.setEnabled(true);
		return flowpack;
	    }
	}, start, length);
	response.setData(flowpacks);
	return response;
    }

    public BaseResponse pageFlowpacks(int customerId, int start, int length) throws RemoteException, CrmUnauthenticatedException, CrmUnknownRelationException {
	BaseResponse response = new BaseResponse();

	List<Flowpack> flowpacks = jdbcTemplate.query("select f.id,f.name,f.icon,f.price,f.catalog,f.hot,p.id is not null as enabled from flowpack f left join flowpack_production p on f.id=p.flow_id left join customer c on p.prepaid=c.prepaid where c.id=? order by f.hot desc,f.priority limit ?,?", new RowMapper<Flowpack>() {
	    @Override
	    public Flowpack mapRow(ResultSet rs, int rowNum) throws SQLException {
		Flowpack flowpack = new Flowpack();
		flowpack.setId(rs.getInt("id"));
		flowpack.setName(rs.getString("name"));
		flowpack.setIcon(rs.getString("icon"));
		flowpack.setPrice(rs.getDouble("price"));
		flowpack.setCatalog(rs.getInt("catalog"));
		flowpack.setHot(rs.getBoolean("hot"));
		flowpack.setEnabled(rs.getBoolean("enabled"));
		return flowpack;
	    }
	}, customerId, start, length);
	response.setData(flowpacks);
	return response;
    }

    /**
     * 获取流量包订购详情.
     *
     * @param customerId 客户唯一标识.
     * @param flowId 流量包ID.
     * @return 流量包详情（包含用户手机号码）
     */
    public Flowpack getFlowpackOrdering(String customerId, int flowId) {
	List<Flowpack> flowpacks = jdbcTemplate.query("select f.id,f.name,f.icon,f.price,p.prepaid,p.offer_id,p.group_id,p.expiration,p.effection,f.catalog,c.mobile\n"
		+ "from flowpack f\n"
		+ "left join flowpack_production p on f.id=p.flow_id\n"
		+ "left join customer c on 1=1\n"
		+ "where c.sim_key=? and f.id=? and c.prepaid=p.prepaid", new RowMapper<Flowpack>() {
		    @Override
		    public Flowpack mapRow(ResultSet rs, int rowNum) throws SQLException {
			Flowpack flowpack = new Flowpack();
			flowpack.setId(rs.getInt("id"));
			flowpack.setName(rs.getString("name"));
			flowpack.setIcon(rs.getString("icon"));
			flowpack.setPrepaid(rs.getBoolean("prepaid"));
			flowpack.setPrice(rs.getDouble("price"));
			flowpack.setOfferId(rs.getString("offer_id"));
			flowpack.setGroupId(rs.getString("group_id"));
			flowpack.setMobile(rs.getString("mobile"));
			flowpack.setCatalog(rs.getInt("catalog"));
			flowpack.setEffection(rs.getInt("effection"));
			flowpack.setExpiration(rs.getInt("expiration"));
			return flowpack;
		    }
		}, customerId, flowId);
	if (flowpacks.size() != 1) {
	    return null;
	}
	return flowpacks.get(0);
    }

    /**
     * 订购流量包. 返回值的{@code code}属性定义如下：
     * <ul>
     * <li>0 —— 订购成功</li>
     * <li>-9 —— 客户未注册</li>
     * <li>-7 —— 流量包ID参数非法</li>
     * <li>-21 —— 预付费用户不能订购后付费流量包、后付费用户不能订购预付费流量包</li>
     * </ul>
     *
     * @param key 客户唯一标识.
     * @param flowId 流量包ID.
     * @param channel 渠道.
     * @return 流量包订购处理结果.
     */
    public BaseResponse orderFlowpack(String key, int flowId,int channel) {
	logger.info("用户（key：{}）订购流量包（id：{}）", key, flowId);
	BaseResponse response = new BaseResponse();
	Customer customer = null;
	Flowpack flowpack = null;
	try {
	    customer = customers.findCustomerByKey(key);
	    if (customer == null) {
		response.setCode(ApplicationException.UNREGIST);
		response.setDescription("用户未注册");
		saveHistory(channel,key, flowId, response.getCode(), response.getDescription());
		return response;
	    }
	    flowpack = findFlowpack(flowId, customer.isPrepaid());
	    if (flowpack == null) {
		response.setCode(ApplicationException.ILLEGAL_ARGS);
		response.setDescription("指定的流量包不存在");
		saveHistory(channel,key, flowId, response.getCode(), response.getDescription());
		return response;
	    }
//            if (hasOrdered(customer.getId())) {
//                response.setCode(ApplicationException.REPLICATION_ORDER);
//                response.setDescription("重复的订购");
//                saveHistory(customer, flowpack, response.getCode(), response.getDescription());
//                return response;
//            }

	    customers.authenticate(customer);
	    customers.linkRelation(customer);
	    crm.order(new OrderRequest(customer, flowpack));
	    saveHistory(channel,customer, flowpack, 0, "订购成功");
	} catch (ApplicationException ex) {
	    logger.warn("订购失败！", ex);
	    response.setCode(ex.getErrorCode());
	    response.setDescription(ex.getMessage());
	    saveHistory(channel,customer, flowpack, response.getCode(), response.getDescription());
	} catch (RemoteException ex) {
	    logger.warn("订购失败！", ex);
	    response.setCode(ApplicationException.NETWORK);
	    response.setDescription("网络异常");
	    saveHistory(channel,customer, flowpack, response.getCode(), response.getDescription());
	} catch (RuntimeException ex) {
	    logger.error("订购失败！", ex);
	    response.setCode(ApplicationException.UNKNOWN);
	    response.setDescription("服务器正忙，请稍候...");
	    saveHistory(channel,customer, flowpack, response.getCode(), response.getDescription());
	}
	return response;
    }

    public Flowpack findFlowpack(int flowId, boolean prepaid) {
	try {
	    return jdbcTemplate.queryForObject("select f.id,f.name,f.icon,f.price,p.prepaid,p.offer_id,p.group_id,p.expiration,p.effection,f.catalog\n"
		    + "from flowpack f\n"
		    + "left join flowpack_production p on f.id=p.flow_id\n"
		    + "where f.id=? and p.prepaid=?", new RowMapper<Flowpack>() {
			@Override
			public Flowpack mapRow(ResultSet rs, int rowNum) throws SQLException {
			    Flowpack flowpack = new Flowpack();
			    flowpack.setId(rs.getInt("id"));
			    flowpack.setName(rs.getString("name"));
			    flowpack.setOfferId(rs.getString("offer_id"));
			    flowpack.setGroupId(rs.getString("group_id"));
			    flowpack.setPrepaid(rs.getBoolean("prepaid"));
			    flowpack.setCatalog(rs.getInt("catalog"));
			    flowpack.setEffection(rs.getInt("effection"));
			    flowpack.setExpiration(rs.getInt("expiration"));
			    return flowpack;
			}
		    }, flowId, prepaid);
	} catch (IncorrectResultSizeDataAccessException ex) {
	    return null;
	}
    }

    public void saveHistory(int channel,String key, int flowId, int code, String description) {
	asyncDao.update("insert into history_order(customer_id,customer_mobile,customer_key,flowpack_id,flowpack_name,prepaid,channel,create_time,status,description)values(-1,'',?,?,'',-1,?,now(),?,?)",
		key,
		flowId,
		channel,
		code,
		description);
    }

    public void saveHistory(int channel,Customer customer, Flowpack flowpack, int code, String description) {
	asyncDao.update("insert into history_order(customer_id,customer_mobile,customer_key,flowpack_id,flowpack_name,prepaid,channel,create_time,status,description)values(?,?,?,?,?,?,?,now(),?,?)",
		customer.getId(),
		customer.getMobile(),
		customer.getKey(),
		flowpack.getId(),
		flowpack.getName(),
		flowpack.isPrepaid(),
		channel,
		code,
		description);
    }

    public void saveWapVisits(String ip, String sim,int channel) {
	asyncDao.update("insert into history_wap(ip,sim,create_time,channel)values(?,?,now(),?)", ip, sim,channel);
    }
}
