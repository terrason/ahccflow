package cn.etuo.ahccflow.module.customer;

import cn.etuo.ahccflow.module.crm.CrmRespondFailureException;
import cn.etuo.ahccflow.module.crm.CrmUnauthenticatedException;
import cn.etuo.ahccflow.module.crm.CrmUnknownRelationException;
import cn.etuo.ahccflow.module.crm.CrmWebService;
import cn.etuo.ahccflow.module.crm.QueryCustomerRequest;
import java.rmi.RemoteException;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {
    private final Logger logger=LoggerFactory.getLogger(CustomerService.class);
    @Autowired
    private CrmWebService crm;
    @Resource
    private JdbcTemplate jdbcTemplate;

    private final RowMapper<Customer> defaultRowMapper = new RowMapper<Customer>() {
        @Override
        public Customer mapRow(ResultSet rs, int rowNum) throws SQLException {
            Customer customer = new Customer();
            customer.setAddress(rs.getString("address"));
            customer.setCertCode(rs.getString("cert_code"));
            customer.setCertName(rs.getString("cert_name"));
            customer.setCertType(rs.getString("cert_type"));
            customer.setContact(rs.getString("contact"));
            customer.setCustId(rs.getString("cust_id"));
            customer.setHcode(rs.getInt("hcode"));
            customer.setId(rs.getInt("id"));
            customer.setKey(rs.getString("sim_key"));
            customer.setMobile(rs.getString("mobile"));
            customer.setName(rs.getString("name"));
            customer.setProductId(rs.getString("product_id"));
            customer.setPrepaid(rs.getBoolean("prepaid"));
            return customer;
        }
    };

    public Customer findCustomer(int id) {
        try {
            return jdbcTemplate.queryForObject("select c.id,c.sim_key,c.mobile,c.cust_id,c.name,c.product_id,c.address,c.contact,c.cert_type,c.cert_name,c.cert_code,c.prepaid,c.hcode from customer c where c.id=?", defaultRowMapper, id);
        } catch (IncorrectResultSizeDataAccessException ex) {
            return null;
        }
    }

    public Customer findCustomerByMobile(String mobile) {
        try {
            return jdbcTemplate.queryForObject("select c.id,c.sim_key,c.mobile,c.cust_id,c.name,c.product_id,c.address,c.contact,c.cert_type,c.cert_name,c.cert_code,c.prepaid,c.hcode from customer c where c.mobile=?", defaultRowMapper, mobile);
        } catch (IncorrectResultSizeDataAccessException ex) {
            return null;
        }
    }

    public Customer findCustomerByKey(String key) {
        try {
            return jdbcTemplate.queryForObject("select c.id,c.sim_key,c.mobile,c.cust_id,c.name,c.product_id,c.address,c.contact,c.cert_type,c.cert_name,c.cert_code,c.prepaid,c.hcode from customer c where c.sim_key=?", defaultRowMapper, key);
        } catch (IncorrectResultSizeDataAccessException ex) {
            return null;
        }
    }

    /**
     * 绑定手机号码与key值. 若手机号码已存在，重新绑定key。
     *
     * @param key key
     * @param mobile 手机号码
     * @return 客户
     */
    public Customer bind(String key, String mobile) throws ReplicateBindingException, UnSupportMobileException {
        logger.info("绑定手机 {} —— {}",key,mobile);
        int exist = jdbcTemplate.queryForObject("select count(1) from customer where mobile=?", Integer.class, mobile);
        if (exist==0) {
            int c = jdbcTemplate.update("insert into customer(sim_key,mobile,hcode) select ?,?,lant_id from section_code where prefix=left(?,7)", key, mobile,mobile);
            if(c==0){
                throw new UnSupportMobileException(mobile);
            }
        } else {
            jdbcTemplate.update("update customer set sim_key=? where mobile=?", key, mobile);
        }

        return findCustomerByMobile(mobile);
    }

    public boolean testRegist(String key) {
        return jdbcTemplate.queryForObject("select count(1) from customer where sim_key = ?", new RowMapper<Boolean>() {

            @Override
            public Boolean mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getInt(1) > 0;
            }
        }, key);
    }

    public void authenticate(Customer customer) throws RemoteException, CrmUnauthenticatedException {
        try {
            if (customer.isAuthenticated()) {
                return;
            }

            logger.info("手机用户认证 {}",customer);
            Customer loginedCustomer = crm.login(new QueryCustomerRequest(customer.getMobile(), customer.getHcode()));
            customer.setCustId(loginedCustomer.getCustId());
            customer.setName(loginedCustomer.getName());
            customer.setAddress(loginedCustomer.getAddress());
            customer.setContact(loginedCustomer.getContact());
            customer.setCertType(loginedCustomer.getCertType());
            customer.setCertName(loginedCustomer.getCertName());
            customer.setCertCode(loginedCustomer.getCertCode());
            if (!customer.isAuthenticated()) {
                throw new CrmUnauthenticatedException(customer);
            }
            jdbcTemplate.update("update customer set cust_id=?,name=?,address=?,contact=?,cert_type=?,cert_name=?,cert_code=? where id=?",
                    customer.getCustId(),
                    customer.getName(),
                    customer.getAddress(),
                    customer.getContact(),
                    customer.getCertType(),
                    customer.getCertName(),
                    customer.getCertCode(),
                    customer.getId());
        } catch (CrmRespondFailureException ex) {
            throw new CrmUnauthenticatedException(customer, ex);
        }
    }

    public void linkRelation(Customer customer) throws RemoteException, CrmUnknownRelationException {
        try {
            if (customer.hasRelation()) {
                return;
            }
            logger.info("手机用户查询销售品关系 {}",customer);
            Customer relatedCustomer = crm.queryRelation(new QueryCustomerRequest(customer.getMobile(), customer.getHcode()));
            customer.setPrepaid(relatedCustomer.isPrepaid());
            customer.setProductId(relatedCustomer.getProductId());
            if (!customer.hasRelation()) {
                throw new CrmUnknownRelationException(customer);
            }
            jdbcTemplate.update("update customer set prepaid=?,product_id=? where id=?", customer.isPrepaid(), customer.getProductId(), customer.getId());
        } catch (CrmRespondFailureException ex) {
            throw new CrmUnknownRelationException(customer, ex);
        }
    }

}
