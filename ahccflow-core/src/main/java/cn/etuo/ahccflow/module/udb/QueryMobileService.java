package cn.etuo.ahccflow.module.udb;

import java.io.IOException;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class QueryMobileService {

    private static final Logger logger = LoggerFactory.getLogger(QueryMobileService.class);
    private static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final byte[] defaultIV = {1, 2, 3, 4, 5, 6, 7, 8};
    @Value("#{settings['udb.url']}")
    private String url = "http://sersh.passport.189.cn/UDBAPPInterface/UDBAPPSYS/MDNIMSIRelation.asmx/QueryMDNByIMSI";
    @Value("#{settings['udb.deviceno']}")
    private String deviceNumber = "3500000000410401";
    @Value("#{settings['udb.key']}")
    private String key = "C92DB4843EA0223162E1BB3E17ACF2B88DAAAA2020699652";
    @Value("#{settings['udb.ext']}")
    private String ext = "tencent";

    public String queryMobileBySim(String sim) throws IOException, UdbRespondFailureException {
        logger.info("查询{}对应的手机号码",sim);
        String timestamp = format.format(new Date());
        Document document = Jsoup.connect(url)
                .ignoreContentType(true)
                .ignoreHttpErrors(true)
                .data("DeviceNo", deviceNumber)
                .data("IMSI", sim)
                .data("StampTime", timestamp)
                .data("Extension", ext)
                .data("Authencitator", endorse(key, deviceNumber, sim, timestamp, ext))
                .post();
        logger.debug("UDB接口返回值：{}",document.body());
        Elements result = document.select("QueryMDNByIMSIResult Result");
        if (result.isEmpty()) {
            throw new RuntimeException("UDB接口返回了不匹配的报文：" + document.body());
        }
        if (!"0".equals(result.text())) {
            throw new UdbRespondFailureException(Integer.parseInt(result.text()), document.text());
        }
        Elements mobileElement = document.select("QueryMDNByIMSIResult MDN");
        String mobile = mobileElement.text();

        if (StringUtils.hasText(mobile)) {
            return mobile;
        } else {
            throw new UdbRespondFailureException(UdbRespondFailureException.UNKNOWN,"udb接口获取到非法的手机号码：“"+mobile+"”");
        }
    }

    private String endorse(String key, String... fields) {
        try {
            logger.debug("签名字段：{}", Arrays.asList(fields));
            logger.debug("签名密钥：{}", key);
            StringBuilder builder = new StringBuilder();
            for (String field : fields) {
                builder.append(field);
            }
            return getAuthenicator(builder.toString(), key);
        } catch (Exception ex) {
            throw new RuntimeException("签名加密算法出错", ex);
        }
    }
//<editor-fold defaultstate="collapsed" desc="加密算法">

    private static String getAuthenicator(String sourceStr, String key) throws Exception {
        Cipher c3des = Cipher.getInstance("DESede/CBC/PKCS5Padding");
        SecretKeySpec myKey = new SecretKeySpec(hexStringToByteArray(key), "DESede");

        IvParameterSpec ivspec = new IvParameterSpec(defaultIV);
        c3des.init(Cipher.ENCRYPT_MODE, myKey, ivspec);

        byte[] testSrc = sourceStr.getBytes();
        MessageDigest alga = MessageDigest.getInstance("SHA-1");
        alga.update(testSrc);
        byte[] digesta = alga.digest();

        byte[] encoded = c3des.doFinal(digesta);
        return Base64.encodeBase64String(encoded);
    }

    private static byte chr2hex(String chr) {
        if (chr.equals("0")) {
            return 0x00;
        } else if (chr.equals("1")) {
            return 0x01;
        } else if (chr.equals("2")) {
            return 0x02;
        } else if (chr.equals("3")) {
            return 0x03;
        } else if (chr.equals("4")) {
            return 0x04;
        } else if (chr.equals("5")) {
            return 0x05;
        } else if (chr.equals("6")) {
            return 0x06;
        } else if (chr.equals("7")) {
            return 0x07;
        } else if (chr.equals("8")) {
            return 0x08;
        } else if (chr.equals("9")) {
            return 0x09;
        } else if (chr.equals("A")) {
            return 0x0a;
        } else if (chr.equals("B")) {
            return 0x0b;
        } else if (chr.equals("C")) {
            return 0x0c;
        } else if (chr.equals("D")) {
            return 0x0d;
        } else if (chr.equals("E")) {
            return 0x0e;
        } else if (chr.equals("F")) {
            return 0x0f;
        }
        return 0x00;
    }

    public static byte[] hexStringToByteArray(String s) {
        byte[] buf = new byte[s.length() / 2];
        for (int i = 0; i < buf.length; i++) {
            buf[i] = (byte) (chr2hex(s.substring(i * 2, i * 2 + 1)) * 0x10 + chr2hex(s
                    .substring(i * 2 + 1, i * 2 + 2)));
        }
        return buf;
    }
//</editor-fold>
    public void setUrl(String url) {
        this.url = url;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }
}
