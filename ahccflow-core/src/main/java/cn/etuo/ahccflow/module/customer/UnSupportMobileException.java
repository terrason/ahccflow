/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.etuo.ahccflow.module.customer;

import cn.etuo.ahccflow.ApplicationException;

public class UnSupportMobileException extends ApplicationException {

    public static final int ERROR_CODE = -26;

    public UnSupportMobileException(String mobile) {
        super("不支持的手机号码：" + mobile.substring(0, 3) + "*****" + mobile.substring(8)+"，您可能不是安徽电信用户！");
        setErrorCode(ERROR_CODE);
    }

    public UnSupportMobileException(String mobile, Throwable cause) {
        super("不支持的手机号码：" + mobile.substring(0, 3) + "*****" + mobile.substring(8)+"，您可能不是安徽电信用户！", cause);
    }
}
