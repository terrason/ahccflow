/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.etuo.ahccflow.module.udb;

import cn.etuo.ahccflow.ApplicationException;

/**
 * 当UDB接口返回错误提示时抛出此异常.
 *
 * @author terrason
 */
public class UdbRespondFailureException extends ApplicationException {

    public UdbRespondFailureException(int code, String message) {
	super("UDB接口返回错误提示：" + message);
	setErrorCode(code > 0 ? code * -1 : code);
    }
}
