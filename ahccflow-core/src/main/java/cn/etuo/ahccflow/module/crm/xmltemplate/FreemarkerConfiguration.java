/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.etuo.ahccflow.module.crm.xmltemplate;

import freemarker.template.Configuration;
import freemarker.template.Template;
import java.io.IOException;
import org.springframework.stereotype.Service;

@Service
public class FreemarkerConfiguration {

    private final Configuration configuration;
    private Template order;
    private Template login;
    private Template queryRelation;

    public FreemarkerConfiguration() {
        Configuration cfg = new Configuration();
        cfg.setDefaultEncoding("UTF-8");
        cfg.setClassForTemplateLoading(getClass(), "");
        configuration = cfg;
    }

    public Template getOrderTemplate() throws IOException {
        if (order == null) {
            order = configuration.getTemplate("order.xml");
        }
        return order;
    }

    public Template getLoginTemplate() throws IOException {
        if (login == null) {
            login = configuration.getTemplate("login.xml");
        }
        return login;
    }
    
    public Template getQueryRelationTemplate() throws IOException {
        if (queryRelation == null) {
            queryRelation = configuration.getTemplate("queryRelation.xml");
        }
        return queryRelation;
    }
}
