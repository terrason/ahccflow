package cn.etuo.ahccflow.controller;

import cn.etuo.ahccflow.ApplicationException;
import cn.etuo.ahccflow.ctmp.qryflowservice.IFQryFlowDto;
import cn.etuo.ahccflow.module.flowquery.QueryFlowService;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping
public class FlowQueryController {

    private static final Logger logger = LoggerFactory.getLogger(FlowQueryController.class);
    @Autowired
    private QueryFlowService service;

    @RequestMapping("/flowQuery")
    @ResponseBody
    public IFQryFlowDto execute(@RequestParam(defaultValue = " > < ||") String key) {
	IFQryFlowDto result;
	try {
	    result = service.queryFlow(key);
	} catch (IOException ex) {
	    logger.warn("查询流量出错", ex);
	    result = new IFQryFlowDto();
	    result.setCode(1);
	    result.setDesc("网络异常");
	} catch (ApplicationException ex) {
	    logger.warn("查询流量出错", ex);
	    result = new IFQryFlowDto();
	    result.setCode(ex.getErrorCode());
	    result.setDesc(ex.getMessage());
	}
	try {
	    service.saveHistory(key, result);
	} catch (Exception e) {
	    logger.warn("记录查询流量结果时出错", e);
	}
	return result;
    }
}
