/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.etuo.ahccflow.controller;

import cn.etuo.ahccflow.ApplicationException;
import cn.etuo.ahccflow.BaseResponse;
import cn.etuo.ahccflow.module.customer.CustomerService;
import cn.etuo.ahccflow.module.customer.Customer;
import cn.etuo.ahccflow.smgp.SMGPCaller;
import com.ytsms.platform.impl.SubmitRespMessage;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class RegistAssistanceController {

    private static final Logger logger = LoggerFactory.getLogger(RegistAssistanceController.class);
    @Autowired
    private CustomerService service;
    @Autowired
    private SMGPCaller smgp;
    @Autowired
    private MessageSource messages;
    private final Random random = new Random();

    @RequestMapping(value = "/testRegist/{key}")
    @ResponseBody
    public boolean isRegistered(@PathVariable String key) {
        return service.testRegist(key);
    }

    @RequestMapping(value = "/smscode/{mobile}")
    @ResponseBody
    public BaseResponse smscode(@PathVariable String mobile) {
        BaseResponse response = new BaseResponse();
        int smscode = random.nextInt(899999) + 100000;
        try {
            SubmitRespMessage r = smgp.submit(messages.getMessage("sms.template", new Object[]{smscode}, Locale.CHINESE), mobile, "");
            response.setCode(r.Status);
            response.setDescription(r.Desc);
            response.setData(smscode);
        } catch (IOException ex) {
            logger.error("发送短信失败", ex);
            response.setCode(ApplicationException.UNKNOWN);
            response.setDescription("服务器出现未知错误");
        } catch (RuntimeException ex) {
            logger.warn("订购失败！", ex);
            response = new BaseResponse();
            response.setCode(ApplicationException.UNKNOWN);
            response.setDescription("服务器正忙，请稍候...");
        }
        return response;
    }

    @RequestMapping(value = "/regist")
    @ResponseBody
    public BaseResponse regist(@RequestParam String mobile, @RequestParam(required = false) String key) {
        BaseResponse response = new BaseResponse();
        if (!StringUtils.hasText(key)) {
            key = UUID.randomUUID().toString();
        }
        response.setData(key);

        try {
            Customer customer = service.bind(key, mobile);
            service.authenticate(customer);
            service.linkRelation(customer);
        } catch (ApplicationException ex) {
            logger.warn("注册失败！", ex);
            response.setCode(ex.getErrorCode());
            response.setDescription(ex.getMessage());
        } catch (RemoteException ex) {
            logger.warn("注册失败！", ex);
            response.setCode(ApplicationException.NETWORK);
            response.setDescription("网络异常");
        } catch (RuntimeException ex) {
            logger.error("注册失败！", ex);
            response.setCode(ApplicationException.UNKNOWN);
            response.setDescription("服务器正忙，请稍候...");
        }

        return response;
    }
}
