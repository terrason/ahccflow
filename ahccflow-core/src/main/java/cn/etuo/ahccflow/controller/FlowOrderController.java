/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.etuo.ahccflow.controller;

import cn.etuo.ahccflow.ApplicationException;
import cn.etuo.ahccflow.BaseResponse;
import cn.etuo.ahccflow.module.customer.Customer;
import cn.etuo.ahccflow.module.customer.CustomerService;
import cn.etuo.ahccflow.module.flowpack.Flowpack;
import cn.etuo.ahccflow.module.flowpack.FlowpackService;
import cn.etuo.ahccflow.module.udb.QueryMobileService;
import cn.etuo.ahccflow.module.udb.UdbRespondFailureException;
import java.io.IOException;
import java.rmi.RemoteException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 订购流量包接口.
 *
 * @author terrason
 */
@Controller
@RequestMapping
public class FlowOrderController {

    private static final Logger logger = LoggerFactory.getLogger(FlowOrderController.class);
    @Autowired
    private FlowpackService service;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private QueryMobileService queryMobileService;

    @RequestMapping(value = "/{channel}/floworder", method = RequestMethod.GET)
    @ResponseBody
    public BaseResponse list(@PathVariable int channel, @RequestParam(defaultValue = "") String ip, Pager pager) {
	BaseResponse result;
	try {
	    result = service.pageFlowpacks(pager.getFirstIndex(), pager.getPageSize());
	} catch (RuntimeException ex) {
	    logger.error("获取流量包订购列表失败", ex);
	    result = new BaseResponse();
	    result.setCode(ApplicationException.UNKNOWN);
	    result.setDescription("服务器出现未知错误");
	}
	try {
	    service.saveWapVisits(ip, "", channel);
	} catch (Exception ex) {
	    logger.warn("保存访问记录出错", ex);
	}
	return result;
    }

    @RequestMapping(value = "/{channel}/floworder/{key}", method = RequestMethod.GET)
    @ResponseBody
    public BaseResponse list(@PathVariable int channel, @PathVariable String key, @RequestParam(defaultValue = "") String ip, Pager pager) {
	BaseResponse result;
	try {
	    try {
		Customer customer = customerService.findCustomerByKey(key);
		if (customer == null) {
		    String mobile = queryMobileService.queryMobileBySim(key);
		    customer = customerService.bind(key, mobile);
		    customerService.authenticate(customer);
		    customerService.linkRelation(customer);
		}
		result = service.pageFlowpacks(customer.getId(), pager.getFirstIndex(), pager.getPageSize());
	    } catch (UdbRespondFailureException ex) {
		logger.warn("获取流量包订购列表失败！", ex);
		result = service.pageFlowpacks(pager.getFirstIndex(), pager.getPageSize());
		result.setCode(401);
		result.setDescription("sim卡号无效");
	    }
	} catch (ApplicationException ex) {
	    logger.warn("获取流量包订购列表失败！", ex);
	    result = new BaseResponse();
	    result.setCode(ex.getErrorCode());
	    result.setDescription(ex.getMessage());
	} catch (IOException ex) {
	    logger.warn("获取流量包订购列表失败！", ex);
	    result = new BaseResponse();
	    result.setCode(ApplicationException.NETWORK);
	    result.setDescription("网络异常");
	} catch (RuntimeException ex) {
	    logger.error("获取流量包订购列表失败", ex);
	    result = new BaseResponse();
	    result.setCode(ApplicationException.UNKNOWN);
	    result.setDescription("服务器出现未知错误");
	}
	try {
	    service.saveWapVisits(ip, key, channel);
	} catch (Exception ex) {
	    logger.warn("保存访问记录出错", ex);
	}
	return result;
    }

    @RequestMapping(value = "/{channel}/floworder/{key}/{flowId}", method = RequestMethod.GET)
    @ResponseBody
    public BaseResponse view(@PathVariable int channel, @PathVariable String key, @PathVariable int flowId
    ) {
	BaseResponse result = new BaseResponse();
	try {
	    Flowpack flowpack = service.getFlowpackOrdering(key, flowId);
	    if (flowpack == null) {
		logger.error("用户（{}）不能订购流量包（{}）", key, flowId);
		result.setCode(ApplicationException.RESOURCE_MISSION);
		result.setDescription("不能处理订购，可能是输入了不合法的参数或用户付费类型不支持");
	    } else {
		result.setData(flowpack);
	    }
	} catch (Exception ex) {
	    logger.error("获取流量包订购列表", ex);
	    result.setCode(ApplicationException.UNKNOWN);
	    result.setDescription("服务器出现未知错误");
	}
	return result;
    }

    @RequestMapping(value = "/{channel}/floworder", method = RequestMethod.POST)
    @ResponseBody
    public BaseResponse order(@PathVariable int channel, @RequestParam String key, @RequestParam int flowId
    ) {
	return service.orderFlowpack(key, flowId, channel);
    }
}
