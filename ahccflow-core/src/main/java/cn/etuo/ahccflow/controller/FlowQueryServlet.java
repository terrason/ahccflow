/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.etuo.ahccflow.controller;

import cn.etuo.ahccflow.ctmp.qryflowservice.IFQryFlowDto;
import cn.etuo.ahccflow.module.flowquery.QueryFlowService;
import java.io.IOException;
import java.io.Writer;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.context.ContextLoader;
/**
 * @deprecated 使用{@link FlowQueryController}代替
 * @author terrason
 */
public class FlowQueryServlet extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(FlowQueryServlet.class);
    private QueryFlowService service;

    public void execute(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        IFQryFlowDto result;
        try {
            String key = req.getParameter("key");
            if (!StringUtils.hasText(key)) {
                return;
            }
            result = getService().queryFlow(key);
        } catch (Exception ex) {
            logger.warn("查询流量出错", ex);
            result = new IFQryFlowDto();
            result.setCode(1);
            result.setDesc("网络异常");
        }
        if (result == null) {
            result = new IFQryFlowDto();
            result.setCode(2);
            result.setDesc("未绑定的手机号");
        }
        logger.debug("查询流量结果：{}",result);
        Writer out = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            resp.setContentType("application/json;charset=utf-8");
            out = resp.getWriter();
            mapper.writeValue(out, result);
            out.flush();
        } catch (IOException ex) {
            logger.warn("返回json数据出错", ex);
        } finally {
            if (out != null) {
                out.close();
            }
        }

    }

    public QueryFlowService getService() {
        if (service == null) {
            service = ContextLoader.getCurrentWebApplicationContext().getBean(QueryFlowService.class);
        }
        return service;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        execute(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        execute(req, resp);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        execute(req, resp);
    }
}
