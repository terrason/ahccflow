/**
 * CrmWebServiceLocator.java
 *
 * This file was auto-generated from WSDL by the Apache Axis 1.4 Apr 22, 2006
 * (06:55:48 PDT) WSDL2Java emitter.
 */
package cn.etuo.ahccflow.ctmp.crmservice;

public class CrmWebServiceLocator extends org.apache.axis.client.Service implements CrmWebService {

    public CrmWebServiceLocator() {
    }

    public CrmWebServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public CrmWebServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }
    // Use to get a borax class for CrmWebServiceHttpPort
//    private java.lang.String CrmWebServiceHttpPort_address = "http://134.64.110.45:9095/crmService/services/crmWebService";
//    private java.lang.String CrmWebServiceHttpPort_address = "http://172.16.7.236:9017/crmService/services/crmWebService";
    private java.lang.String CrmWebServiceHttpPort_address;

    public java.lang.String getCrmWebServiceHttpPortAddress() {
        return CrmWebServiceHttpPort_address;
    }
    // The WSDD service name defaults to the port name.
    private java.lang.String CrmWebServiceHttpPortWSDDServiceName = "CrmWebServiceHttpPort";

    public java.lang.String getCrmWebServiceHttpPortWSDDServiceName() {
        return CrmWebServiceHttpPortWSDDServiceName;
    }

    public void setCrmWebServiceHttpPortWSDDServiceName(java.lang.String name) {
        CrmWebServiceHttpPortWSDDServiceName = name;
    }

    public CrmWebServicePortType getCrmWebServiceHttpPort() throws javax.xml.rpc.ServiceException {
        java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(CrmWebServiceHttpPort_address);
        } catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getCrmWebServiceHttpPort(endpoint);
    }

    public CrmWebServicePortType getCrmWebServiceHttpPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            CrmWebServiceHttpBindingStub _stub = new CrmWebServiceHttpBindingStub(portAddress, this);
            _stub.setPortName(getCrmWebServiceHttpPortWSDDServiceName());
            return _stub;
        } catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setCrmWebServiceHttpPortEndpointAddress(java.lang.String address) {
        CrmWebServiceHttpPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation. If this service has
     * no port for the given interface, then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (CrmWebServicePortType.class.isAssignableFrom(serviceEndpointInterface)) {
                CrmWebServiceHttpBindingStub _stub = new CrmWebServiceHttpBindingStub(new java.net.URL(CrmWebServiceHttpPort_address), this);
                _stub.setPortName(getCrmWebServiceHttpPortWSDDServiceName());
                return _stub;
            }
        } catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation. If this service has
     * no port for the given interface, then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("CrmWebServiceHttpPort".equals(inputPortName)) {
            return getCrmWebServiceHttpPort();
        } else {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://service.WSSInterface.crm.tydic.com", "CrmWebService");
    }
    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://service.WSSInterface.crm.tydic.com", "CrmWebServiceHttpPort"));
        }
        return ports.iterator();
    }

    /**
     * Set the endpoint address for the specified port name.
     */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {

        if ("CrmWebServiceHttpPort".equals(portName)) {
            setCrmWebServiceHttpPortEndpointAddress(address);
        } else { // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
     * Set the endpoint address for the specified port name.
     */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }
}
