package cn.etuo.ahccflow.ctmp.qryflowservice;

import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.rpc.ServiceException;

public class QryFlowUtil {

    private String url;

    /**
     *
     * <p>Title: </p>
     * <p>Description: 获取webservice客户端实例</p>
     *
     * @author：Administrator
     * @date 2013-6-6 下午7:26:48
     * @param:
     * @return: CrmWebServicePortType
     * @throws:
     */
    public GetRatableQryPortType getClient() throws RemoteException {
        try {
            GetRatableQry_ServiceLocator service = new GetRatableQry_ServiceLocator();
            service.setgetRatableQryHttpPortEndpointAddress(url);
            GetRatableQryPortType client = service.getgetRatableQryHttpPort();
            return client;
        } catch (ServiceException ex) {
            throw new RemoteException(ex.getMessage(),ex);
        }
    }

    public IFQryFlowDto queryFlow(String mobile, String date, String hcode) throws RemoteException {//不抛出异常怎么处理错误啊
        String2StringMapEntry[] entry = {
            new String2StringMapEntry("nStaffId", "100099"),
            new String2StringMapEntry("nLantId", hcode),
            new String2StringMapEntry("sServiceNbr", mobile),
            new String2StringMapEntry("nBillingCycleId", date),
            new String2StringMapEntry("sAcctType", "1")
        };
        IFQryFlowDto qry = new IFQryFlowDto();
        String2StringMapEntry[] response = getClient().getRatableQry(entry);
        if (response != null) {
            String error = response[1].getValue();
            if ("0".equals(error)) {
                String data = response[2].getValue();
                if (data != null && !"".equals(data)) {
                    double total = 0.00;
                    double used = 0.00;
                    double leave = 0.00;
                    double over = 0.00;
                    String[] rowDatas = data.split("##");
                    for (String rowData : rowDatas) {
                        String[] colDatas = rowData.split("&&");
                        if ("MB流量".equals(colDatas[6])) {
                            double ototal = Double.valueOf(colDatas[3]);
                            double oused = Double.valueOf(colDatas[4]);
                            double oleave = Double.valueOf(colDatas[5]);
                            total += ototal;
                            used += oused;
                            leave += oleave;
                            if (oleave == 0) {
                                over += oused - ototal;
                            }
                        }
                    }
                    qry.setCode(0);
                    qry.setUsed(used);
                    qry.setTotal(total);
                    qry.setLeave(leave);
                    qry.setOver(over);
                    return qry;
                }
            }
        }
        qry.setCode(1);
        return qry;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
