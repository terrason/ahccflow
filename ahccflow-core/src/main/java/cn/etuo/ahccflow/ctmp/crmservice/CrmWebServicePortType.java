/**
 * CrmWebServicePortType.java
 *
 * This file was auto-generated from WSDL by the Apache Axis 1.4 Apr 22, 2006
 * (06:55:48 PDT) WSDL2Java emitter.
 */
package cn.etuo.ahccflow.ctmp.crmservice;

public interface CrmWebServicePortType extends java.rmi.Remote {

    public java.lang.String query(java.lang.String in0) throws java.rmi.RemoteException;

    public java.lang.String callService(java.lang.String in0) throws java.rmi.RemoteException;
}
