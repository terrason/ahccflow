package cn.etuo.ahccflow.ctmp.crmservice;

public class Group implements java.io.Serializable {

    private String groupId;
    private String offId;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getOffId() {
        return offId;
    }

    public void setOffId(String offId) {
        this.offId = offId;
    }
}
