/**
 * GetRatableQryPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cn.etuo.ahccflow.ctmp.qryflowservice;

public interface GetRatableQryPortType extends java.rmi.Remote {
    public cn.etuo.ahccflow.ctmp.qryflowservice.String2StringMapEntry[] getRatableQry(cn.etuo.ahccflow.ctmp.qryflowservice.String2StringMapEntry[] in0) throws java.rmi.RemoteException;
}
