/**
 * GetRatableQry_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cn.etuo.ahccflow.ctmp.qryflowservice;


public class GetRatableQry_ServiceLocator extends org.apache.axis.client.Service implements cn.etuo.ahccflow.ctmp.qryflowservice.GetRatableQry_Service {

    public GetRatableQry_ServiceLocator() {
    }


    public GetRatableQry_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public GetRatableQry_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for getRatableQryHttpPort
    private java.lang.String getRatableQryHttpPort_address;

    public java.lang.String getgetRatableQryHttpPortAddress() {
        return getRatableQryHttpPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String getRatableQryHttpPortWSDDServiceName = "getRatableQryHttpPort";

    public java.lang.String getgetRatableQryHttpPortWSDDServiceName() {
        return getRatableQryHttpPortWSDDServiceName;
    }

    public void setgetRatableQryHttpPortWSDDServiceName(java.lang.String name) {
        getRatableQryHttpPortWSDDServiceName = name;
    }

    public cn.etuo.ahccflow.ctmp.qryflowservice.GetRatableQryPortType getgetRatableQryHttpPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(getRatableQryHttpPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getgetRatableQryHttpPort(endpoint);
    }

    public cn.etuo.ahccflow.ctmp.qryflowservice.GetRatableQryPortType getgetRatableQryHttpPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            cn.etuo.ahccflow.ctmp.qryflowservice.GetRatableQryHttpBindingStub _stub = new cn.etuo.ahccflow.ctmp.qryflowservice.GetRatableQryHttpBindingStub(portAddress, this);
            _stub.setPortName(getgetRatableQryHttpPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setgetRatableQryHttpPortEndpointAddress(java.lang.String address) {
        getRatableQryHttpPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (cn.etuo.ahccflow.ctmp.qryflowservice.GetRatableQryPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                cn.etuo.ahccflow.ctmp.qryflowservice.GetRatableQryHttpBindingStub _stub = new cn.etuo.ahccflow.ctmp.qryflowservice.GetRatableQryHttpBindingStub(new java.net.URL(getRatableQryHttpPort_address), this);
                _stub.setPortName(getgetRatableQryHttpPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("getRatableQryHttpPort".equals(inputPortName)) {
            return getgetRatableQryHttpPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://getRatableQry.tydic.com", "getRatableQry");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://getRatableQry.tydic.com", "getRatableQryHttpPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("getRatableQryHttpPort".equals(portName)) {
            setgetRatableQryHttpPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
