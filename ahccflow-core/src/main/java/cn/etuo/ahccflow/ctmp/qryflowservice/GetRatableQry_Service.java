/**
 * GetRatableQry_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cn.etuo.ahccflow.ctmp.qryflowservice;

public interface GetRatableQry_Service extends javax.xml.rpc.Service {
    public java.lang.String getgetRatableQryHttpPortAddress();

    public cn.etuo.ahccflow.ctmp.qryflowservice.GetRatableQryPortType getgetRatableQryHttpPort() throws javax.xml.rpc.ServiceException;

    public cn.etuo.ahccflow.ctmp.qryflowservice.GetRatableQryPortType getgetRatableQryHttpPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
