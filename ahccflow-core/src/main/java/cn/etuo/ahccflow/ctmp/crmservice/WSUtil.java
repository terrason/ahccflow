package cn.etuo.ahccflow.ctmp.crmservice;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.xml.rpc.ServiceException;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import cn.etuo.common.util.FileUtil;
public class WSUtil {

    private Logger log = LoggerFactory.getLogger(WSUtil.class);
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private String url;

    public WSUtil(String url) {
        this.url = url;
    }
    /**
     *
     * <p>Title: </p>
     * <p>Description: 获取webservice客户端实例</p>
     *
     * @author：Administrator
     * @date 2013-6-6 下午7:26:48
     * @param:
     * @return: CrmWebServicePortType
     * @throws:
     */
    public CrmWebServicePortType getClient() {
        CrmWebServiceLocator service = new CrmWebServiceLocator();
        service.setCrmWebServiceHttpPortEndpointAddress(url);
        CrmWebServicePortType client = null;
        try {
            client = service.getCrmWebServiceHttpPort();
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return client;
    }

    public String createLoginReqXml(String mobile, int latnId) {
        String reqXml = "<?xml version='1.0' encoding='GB18030'?>"
                + "<ESB><TIME>1264568241221</TIME>"
                + "<RECORD_TOTAL>0</RECORD_TOTAL>"
                + "<contractBody>"
                + "<Login>"
                + "<serviceNbr>" + mobile + "</serviceNbr>"
                + "<latnId>" + latnId + "</latnId>"
                + "<loginType>7</loginType>"
                + "<passWord></passWord>"
                + "</Login>"
                + "</contractBody>"
                + "<TO>CRM</TO>"
                + "<WSS_LATN_ID>" + latnId + "</WSS_LATN_ID>"
                + "<sysId>1041</sysId>"
                + "<CURRENT_PAGE>0</CURRENT_PAGE>"
                + "<PAGE_SIZE>0</PAGE_SIZE>"
                + "<MSG_ID>1264568241221</MSG_ID>"
                + "<SERVICE_CD>quePty00001</SERVICE_CD>"
                + "<FROM>SHARE</FROM>"
                + "</ESB>";
        return reqXml;
    }

    public String createGroupReqXml(String mobile, int latnId) {
        String reqXml = "<?xml version='1.0' encoding='GB18030'?>"
                + "<ESB><TIME>1288571532277</TIME>"
                + "<RECORD_TOTAL>0</RECORD_TOTAL>"
                + "<contractBody>"
                + "<QueryCustOfr>"
                + "<serviceNbr>" + mobile + "</serviceNbr>"
                + "<latnId>" + latnId + "</latnId>"
                + "<type>2</type>"
                + "<loginType>4</loginType>"
                + "</QueryCustOfr>"
                + "</contractBody>"
                + "<TO>CRM</TO>"
                + "<WSS_LATN_ID>" + latnId + "</WSS_LATN_ID>"
                + "<sysId>1041</sysId>"
                + "<CURRENT_PAGE>0</CURRENT_PAGE>"
                + "<PAGE_SIZE>0</PAGE_SIZE>"
                + "<MSG_ID>1288571532277</MSG_ID>"
                + "<SERVICE_CD>quePty00003</SERVICE_CD>"
                + "<FROM>SHARE</FROM>"
                + "</ESB>";
        return reqXml;
    }

    public String createOrderReqXml(Customer cus, String productId, String offerProductId, String groupId,
            String mobile, int latnId, int effect, int inactive, boolean isFree) {
        String reqXml = "<?xml version='1.0' encoding='GB18030'?>"
                + "<ESB><TIME>1271815864211</TIME>"
                + "<RECORD_TOTAL>0</RECORD_TOTAL>"
                + "<contractBody>"
                + "<OfferAccept>"
                + "<offeringProductId>" + offerProductId + "</offeringProductId>"
                + "<productSets>"
                + "<productSet>"
                + "<serviceNbr>" + mobile + "</serviceNbr>"
                + "<offeringProductId>481</offeringProductId>"
                + "<attrProductSets></attrProductSets>"
                + "<productId>" + productId + "</productId>"
                + "<attachProdSets>"
                + "</attachProdSets>"
                + "</productSet>"
                + "</productSets>"
                + "<latnId>" + latnId + "</latnId>"
                + "<inactiveDate>" + inactive + "</inactiveDate>"
                + "<isRealOrder>0</isRealOrder>";
        if (isFree) {
            reqXml += "<developer>"
                    + "<developerSet>"
                    + "<developerId>安徽一拓通信科技集团有限公司</developerId>"
                    + "<developerName>AH00F00038</developerName>"
                    + "</developerSet>"
                    + "</developer>";
        }
        reqXml += "<custInfo>"
                + "<certName>" + cus.getCertName() + "</certName>"
                + "<certCode>" + cus.getCertCode() + "</certCode>"
                + "<certType>" + cus.getCertType() + "</certType>"
                + "<contactNumber></contactNumber>"
                + "<custId>" + cus.getCustId() + "</custId>"
                + "<email></email>"
                + "<custAddress>" + cus.getCustAddress() + "</custAddress>"
                + "<cusName>" + cus.getCusName() + "</cusName>"
                + "</custInfo>"
                + "<effectDate>" + effect + "</effectDate>"
                + "<attrOfferingSets>"
                + "<attrOfferingSet>"
                + "<attrOfferingValue>" + groupId + "</attrOfferingValue>"
                + "<attrOfferingCode>GROUP_ID</attrOfferingCode>"
                + "<attrOfferingName>策略组</attrOfferingName>"
                + "</attrOfferingSet>"
                + "</attrOfferingSets>"
                + "</OfferAccept>"
                + "</contractBody>"
                + "<TO>CRM</TO>"
                + "<WSS_LATN_ID>" + latnId + "</WSS_LATN_ID>"
                + "<sysId>1041</sysId>"
                + "<CURRENT_PAGE>0</CURRENT_PAGE>"
                + "<PAGE_SIZE>0</PAGE_SIZE>"
                + "<MSG_ID>1271815864211</MSG_ID>"
                + "<SERVICE_CD>exeBuss00003</SERVICE_CD>"
                + "<FROM>SHARE</FROM>"
                + "</ESB>";
        return reqXml;
    }

    public String createIsCanOrderReqXml(String offerProductdId, String mobile, int latnId, String custId) {
        String reqXml = "<?xml version='1.0' encoding='GB18030'?>"
                + "<ESB><TIME>1265265741961</TIME>"
                + "<RECORD_TOTAL>0</RECORD_TOTAL>"
                + "<contractBody>"
                + "<CheckOfrAccept>"
                + "<offeringProductId>" + offerProductdId + "/offeringProductId>"
                + "<latnId>" + latnId + "</latnId>"
                + "<optType>1</optType>"
                + "<custId>" + custId + "</custId>"
                + "</CheckOfrAccept>"
                + "</contractBody>"
                + "<TO>CRM</TO>"
                + "<WSS_LATN_ID>" + latnId + "</WSS_LATN_ID>"
                + "<sysId>1041</sysId>"
                + "<CURRENT_PAGE>0</CURRENT_PAGE>"
                + "<PAGE_SIZE>0</PAGE_SIZE>"
                + "<MSG_ID>1265265741961</MSG_ID>"
                + "<SERVICE_CD>exeBuss00019</SERVICE_CD>"
                + "<FROM>SHARE</FROM>"
                + "</ESB>";
        return reqXml;
    }

    public String createCancelReqXml(Customer cus, int latnId, String offId, String offProductId, String groupId) {
        String reqXml = "<?xml version='1.0' encoding='GB18030'?>"
                + "<ESB><TIME>1265003753437</TIME>"
                + "<RECORD_TOTAL>0</RECORD_TOTAL>"
                + "<contractBody>"
                + "<exeBuss00005>"
                + "<offeringProductId>" + offProductId + "</offeringProductId>"
                + "<latnId>" + latnId + "</latnId>"
                + "<custInfo>"
                + "<certName>" + cus.getCertName() + "</certName>"
                + "<certCode>" + cus.getCertCode() + "</certCode>"
                + "<certType>" + cus.getCertType() + "</certType>"
                + "<contactNumber></contactNumber>"
                + "<custId>" + cus.getCustId() + "</custId>"
                + "<custAddress>" + cus.getCustAddress() + "</custAddress>"
                + "<cusName>" + cus.getCusName() + "</cusName>"
                + "</custInfo>"
                + "<offeringId>" + offId + "</offeringId>"
                + "<attrOfferingSets>"
                + "<attrOfferingSet>"
                + "<attrOfferingValue>" + groupId + "</attrOfferingValue>"
                + "<attrOfferingCode>GROUP_ID</attrOfferingCode>"
                + "<attrOfferingName>策略组</attrOfferingName>"
                + "</attrOfferingSet>"
                + "<attrOfferingSet>"
                + "<attrOfferingValue></attrOfferingValue>"
                + "<attrOfferingCode>DISCT_RULE_ID</attrOfferingCode>"
                + "<attrOfferingName>优惠规则</attrOfferingName>"
                + "</attrOfferingSet>"
                + "</attrOfferingSets>"
                + "<effectDate>0</effectDate>"
                + "</exeBuss00005>"
                + "</contractBody>"
                + "<TO>CRM</TO>"
                + "<WSS_LATN_ID>" + latnId + "</WSS_LATN_ID>"
                + "<sysId>1041</sysId>"
                + "<CURRENT_PAGE>0</CURRENT_PAGE>"
                + "<PAGE_SIZE>0</PAGE_SIZE>"
                + "<MSG_ID>1265003753437</MSG_ID>"
                + "<SERVICE_CD>exeBuss00005</SERVICE_CD>"
                + "<FROM>SHARE</FROM>"
                + "</ESB>";

        return reqXml;
    }

    /**
     *
     * <p>Title: </p>
     * <p>Description: 登录</p>
     *
     * @author：Administrator
     * @date 2013-6-20 下午4:15:51
     * @param:
     * @return: void
     * @throws DocumentException
     * @throws:
     */
    public Customer login(String mobile, int latnId) throws RemoteException, DocumentException {
        String reqXml = createLoginReqXml(mobile, latnId);
        log.info("登录请求报文：" + reqXml);
        //调用登录接口获取用户信息
        //Date sdate = new Date();
        //new FileUtil().writeReqPath("CRM登录请求 ："+sdf.format(new Date())+" || quePty00001 || "+mobile+" || " + latnId);
        //String debugTxt = "线程ID："+Thread.currentThread().getId()+" || 接口名称：quePty00001 || 开始时间："+sdf.format(sdate) + " || ";
        String response = getClient().callService(reqXml);
        //Date edate = new Date();
        //debugTxt += "结束时间："+sdf.format(edate)+" || 耗时："+(edate.getTime()-sdate.getTime())+"\r\n";
        //new FileUtil().writeCrmTime(debugTxt);
        log.info("登录返回报文：" + response);
        Document doc = DocumentHelper.parseText(response);
        Node loginNode = doc.selectSingleNode("/ESB/contractBody/Login");
        if (loginNode != null) {
            Node resultNode = loginNode.selectSingleNode("result");
            if (resultNode != null) {
                int result = Integer.valueOf(resultNode.getText());
                if (result == 0) {
                    Customer cus = new Customer();
                    cus.setCustId(loginNode.selectSingleNode("custId").getText());
                    cus.setCusName(loginNode.selectSingleNode("cusName").getText());
                    cus.setCustAddress(loginNode.selectSingleNode("custAddress").getText());
                    cus.setContactNumber(loginNode.selectSingleNode("contactNumber").getText());
                    cus.setCertType(loginNode.selectSingleNode("certType").getText());
                    cus.setCertName(loginNode.selectSingleNode("certName").getText());
                    cus.setCertCode(loginNode.selectSingleNode("certCode").getText());
                    log.info("登录成功");
                    return cus;
                }
            }
        }
        return null;
    }

    /**
     *
     * <p>Title: </p>
     * <p>Description: 订购关系</p>
     *
     * @author：Administrator
     * @date 2013-6-20 下午5:15:19
     * @param:
     * @return: ProInfo
     * @throws:
     */
    public ProInfo getProInfo(String mobile, int latnId) throws RemoteException, DocumentException {
        String reqXml = createGroupReqXml(mobile, latnId);
        log.info("订购关系请求报文：" + reqXml);
        //new FileUtil().writeReqPath("CRM订购关系请求 ："+sdf.format(new Date())+" || quePty00003 || "+mobile+" || " + latnId);
        //Date sdate = new Date();
        //String debugTxt = "线程ID："+Thread.currentThread().getId()+" || 接口名称：quePty00003 || 开始时间："+sdf.format(sdate) + " || ";
        String response = getClient().callService(reqXml);
        //Date edate = new Date();
        //debugTxt += "结束时间："+sdf.format(edate)+" || 耗时："+(edate.getTime()-sdate.getTime())+"\r\n";
        //new FileUtil().writeCrmTime(debugTxt);
        log.info("订购关系返回报文：" + response);
        Document doc = DocumentHelper.parseText(response);
        Node resultNode = doc.selectSingleNode("/ESB/contractBody/QueryCustOfr/result");
        int result = Integer.valueOf(resultNode.getText());
        ProInfo pro = null;
        List<Group> groups = null;
        Group group = null;
        if (result == 0) {
            groups = new ArrayList<Group>();
            pro = new ProInfo();
            List<Node> offSets = doc.selectNodes("/ESB/contractBody/QueryCustOfr/orderedOfferingSets/orderedOfferingSet");
            for (Node offSet : offSets) {
                List<Node> proSets = offSet.selectNodes("prodSets/prodSet");
                for (Node proSet : proSets) {
                    //<editor-fold defaultstate="collapsed" desc="comment">
                    String serviceNbr = proSet.selectSingleNode("serviceNbr").getText();
                    if (mobile.equals(serviceNbr)) {
                        if (pro.getProductId() == null) {
                            pro.setProductId(proSet.selectSingleNode("productId").getText());
                            //							System.out.println("productId:" + pro.getProductId());
                        }
                        if (pro.getIsPrepay() == -1) {
                            String isPrePay = proSet.selectSingleNode("isPrepay").getText();
                            pro.setIsPrepay(Integer.valueOf(isPrePay));
                            //							System.out.println("isPrePay:" + pro.getIsPrepay());
                        }
                        break;
                    }
                    //</editor-fold>
                }

                List<Node> offrAttrs = offSet.selectNodes("offeringAttrSets/offeringAttrSet");

                for (Node offrAttr : offrAttrs) {
                    //<editor-fold defaultstate="collapsed" desc="comment">
                    String tag = offrAttr.selectSingleNode("offeringAttrCode").getText();
                    if (tag.equals("GROUP_ID")) {
                        String groupId = offrAttr.selectSingleNode("offeringAttrValue").getText();
                        pro.setGroupIds(groupId);
                        String offId = offSet.selectSingleNode("offeringId").getText();
                        group = new Group();
                        group.setGroupId(groupId);
                        group.setOffId(offId);
                        groups.add(group);
                        break;
                    }
                    //</editor-fold>
                }
                String offId = offSet.selectSingleNode("offeringProductId").getText();
                pro.setOfferingProductIds(offId);
            }
            pro.setGroups(groups);
        }
        return pro;
    }

    /**
     *
     * <p>Title: </p>
     * <p>Description: 开通业务</p>
     *
     * @author：Administrator
     * @date 2013-6-22 上午11:51:23
     * @param:
     * @return: boolean
     * @throws:
     */
    public boolean order(Customer cus, String productId, String offerProductId, String groupId, String mobile, int latnId, boolean isFree) throws RemoteException, DocumentException {
        int actDate = 99;
        if (isFree) {
            actDate = 1;
        }
        String reqXml = createOrderReqXml(cus, productId, offerProductId, groupId, mobile, latnId, 0, actDate, isFree);
        log.info("套餐办理请求报文：" + reqXml);
        //new FileUtil().writeReqPath("CRM业务开通请求 ："+sdf.format(new Date())+" || exeBuss00003 || "+mobile+" || " + latnId + " || " + offerProductId + " || " + groupId);
        //Date sdate = new Date();
        //String debugTxt = "线程ID："+Thread.currentThread().getId()+" || 接口名称：exeBuss00003 || 开始时间："+sdf.format(sdate) + " || ";
        String response = getClient().callService(reqXml);
        //Date edate = new Date();
        //debugTxt += "结束时间："+sdf.format(edate)+" || 耗时："+(edate.getTime()-sdate.getTime())+"\r\n";
        //new FileUtil().writeCrmTime(debugTxt);
        log.info("套餐办理返回报文：" + response);
        Document doc = DocumentHelper.parseText(response);
        int result = Integer.valueOf(doc.selectSingleNode("/ESB/contractBody/OfferAccept/result").getText());
        if (result == 0) {
            return true;
        }
        return false;
    }

    /**
     *
     * <p>Title: </p>
     * <p>Description: 取消业务</p>
     *
     * @author：Administrator
     * @date 2013-6-22 上午11:52:00
     * @param:
     * @return: boolean
     * @throws RemoteException
     * @throws DocumentException
     * @throws:
     */
    public boolean cancel(Customer cus, int latnId, String offId, String offProductId, String groupId) throws RemoteException, DocumentException {
        String reqXml = createCancelReqXml(cus, latnId, offId, offProductId, groupId);
        log.info("取消套餐请求报文：" + reqXml);
        String response = getClient().callService(reqXml);
        log.info("取消套餐返回报文：" + response);
        Document doc = DocumentHelper.parseText(response);
        int result = Integer.valueOf(doc.selectSingleNode("/ESB/contractBody/OfferCancel/result").getText());
        if (result == 0) {
            return true;
        }
        return false;
    }

    public boolean startOrder(String offerProductId, String groupId, String mobile, int latnId, boolean isFree, int isPrepay) {
        try {
            ProInfo pro = getProInfo(mobile, latnId);
            if (pro != null && isPrepay != pro.getIsPrepay()) {
                return false;
            }
            if (pro != null && pro.getOfferingProductIds().indexOf(offerProductId) != -1) {
                return false;
            }
            Customer cus = login(mobile, latnId);
            if (cus != null) {
                return order(cus, pro.getProductId(), offerProductId, groupId, mobile, latnId, isFree);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        return false;
    }

    public OrderResponse selfOrder(String mobile, int latnId, String offerProductId, String groupId, int effect, int inactive, int isPrepay, boolean isFree) {
        OrderResponse response = new OrderResponse();
        try {
            ProInfo pro = getProInfo(mobile, latnId);
            if (pro == null || pro.getProductId() == null || "".equals(pro.getProductId())) {
                response.setCode(1);
                response.setDesc("订购关系获取失败");
            } else {
                if (isPrepay != pro.getIsPrepay()) {
                    response.setCode(1);
                    if (pro.getIsPrepay() == 0) {
                        response.setDesc("后付费用户无法办理预付费流量包");
                    } else {
                        response.setDesc("预付费用户无法办理后付费流量包");
                    }
                } else {
                    Customer cus = login(mobile, latnId);
                    if (cus == null) {
                        response.setCode(1);
                        response.setDesc("登录失败");
                    } else {
                        String reqXml = createOrderReqXml(cus, pro.getProductId(), offerProductId, groupId, mobile, latnId, effect, inactive, isFree);
//						new FileUtil().writeFlowInfo(mobile + ">>>" + sdf.format(new Date()) + ">>>>" + "订购请求报文:\r\n" + reqXml + "\r\n");
                        String res = getClient().callService(reqXml);
//						new FileUtil().writeFlowInfo(mobile + ">>>" + sdf.format(new Date()) + ">>>>" + "订购返回报文:\r\n" + res + "\r\n");
                        Document doc = DocumentHelper.parseText(res);
                        int result = Integer.valueOf(doc.selectSingleNode("/ESB/contractBody/OfferAccept/result").getText());
                        if (result == 0) {
                            response.setDesc("成功");
                        } else {
                            response.setCode(1);
                            response.setDesc(doc.selectSingleNode("/ESB/contractBody/OfferAccept/msg").getText());
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.setCode(1);
            response.setDesc("订购失败");
        }
        return response;
    }

    public String selfCancel(String mobile, int latnId, String offerProductId, String groupId) {
        try {
            ProInfo pro = getProInfo(mobile, latnId);
            if (pro == null) {
                return "订购关系获取失败";
            } else {
                Customer cus = login(mobile, latnId);
                if (cus == null) {
                    return "登录失败";
                } else {
                    String offId = "";
                    List<Group> groups = pro.getGroups();
                    for (Group group : groups) {
                        if (group.getGroupId().equals(groupId)) {
                            offId = group.getOffId();
                            break;
                        }
                    }
                    String reqXml = createCancelReqXml(cus, latnId, offId, offerProductId, groupId);
                    return getClient().callService(reqXml);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "取消失败";
        }
    }

    public String getHasBuyedProId(String mobile, int latnId) {
        try {
            ProInfo pro = getProInfo(mobile, latnId);
            return "已订购流量包的productId:" + pro.getOfferingProductIds();
        } catch (Exception e) {
            e.printStackTrace();
            return "获取已订购数据失败";
        }
    }

    /**
     * <p>Title: </p>
     * <p>Description: </p>
     *
     * @author：Administrator
     * @date 2013-6-6 下午7:22:43
     * @param:
     * @return: void
     * @throws:
     */
    public static void main(String[] args) {
        WSUtil wsUtil = new WSUtil("http://134.64.110.45:9095/crmService/services/crmWebService");
        Customer customer = new Customer();
        customer.setCertCode("${customer.code}");
        customer.setCertName("${customer.name}");
        customer.setCertType("${customer.type}");
        customer.setContactNumber("${customer.phone}");
        customer.setCusName("${customer.name}");
        customer.setCustAddress("${customer.address}");
        customer.setCustId("${customer.id}");
        customer.setUserId(-1001);
        String orderRequestXml = wsUtil.createOrderReqXml(customer, "$productId", "$offerProductId", "$groupId", "$mobile", -551, -1, -999, true);
        System.out.println(orderRequestXml);
//		System.out.println(wsUtil.startOrder("500067700", "1153821", "18056080891", 551));
//        try {
//            ProInfo pro = wsUtil.getProInfo("", 0);
////			boolean flag = wsUtil.startOrder(pro.getProductId(), "", "", "", 0);
////			System.out.println("aa:" + flag);
//        } catch (RemoteException e) {
//            e.printStackTrace();
//        } catch (DocumentException e) {
//            e.printStackTrace();
//        }
    }
    private String loginResponse = "<?xml version=\"1.0\" encoding=\"GB18030\"?>"
            + "<ESB>"
            + "<MSG_ID/>"
            + "<SERVICE_CD>quePty00001</SERVICE_CD>"
            + "<FROM>CRM</FROM>"
            + "<TO>WSS</TO>"
            + "<TIME>20100127125722</TIME>"
            + "<RETURN_CD desc=\"返回0成功，返回1失败\">0</RETURN_CD>"
            + "<RETURN_DESC/>"
            + "<contractBody>"
            + "<Login>"
            + "<result>0</result>"
            + "<errCode></errCode>"
            + "<msg></msg>"
            + "<custId>55101938620</custId>"
            + "<cusName>&#29579;&#20161;&#24544;</cusName>"
            + "<type>1</type>"
            + "<custClass>24</custClass>"
            + "<sex></sex>"
            + "<custAddress>&#38271;&#23703;&#20065;&#22823;&#24217;&#26449;&#27827;&#21271;&#38431;</custAddress>"
            + "<contactNumber></contactNumber>"
            + "<certType></certType>"
            + "<certName></certName>"
            + "<certCode></certCode>"
            + "<industryCd>56</industryCd>"
            + "<industryName>&#20854;&#20182;&#34892;&#19994;</industryName>"
            + "<areaCode>0551</areaCode>"
            + "<custType>23</custType>"
            + "<custTypeName>&#32479;&#21253;&#23458;&#25143;</custTypeName>"
            + "<brandType></brandType>"
            + "<brandTypeName></brandTypeName>"
            + "<custModifyInfoSets>"
            + "<custModifyInfoSet>"
            + "<attrTypeCode>custPassword	</attrTypeCode>"
            + "<attrTypeName>&#23458;&#25143;&#23494;&#30721;</attrTypeName>"
            + "<value>654321</value>"
            + "</custModifyInfoSet>"
            + "<custModifyInfoSet>"
            + "<attrTypeCode>effDate</attrTypeCode>"
            + "<attrTypeName>&#20837;&#32593;&#26102;&#38388;</attrTypeName>"
            + "<value>2006-12-30</value>"
            + "</custModifyInfoSet>"
            + "</custModifyInfoSets>"
            + "<memberClass/>"
            + "</Login>"
            + "</contractBody>"
            + "</ESB>";
    public String groupResponse = "<?xml version=\"1.0\" encoding=\"GB18030\"?>"
            + "<ESB>"
            + "	<MSG_ID>1289956071892</MSG_ID>"
            + "	<SERVICE_CD>quePty00003</SERVICE_CD>"
            + "	<FROM>CRM</FROM>"
            + "	<TO>WSS</TO>"
            + "	<TIME>20101117090751</TIME>"
            + "	<RETURN_CD desc=\"返回0成功，返回1失败\">0</RETURN_CD>"
            + "	<RETURN_DESC/>"
            + "	<contractBody>"
            + "		<QueryCustOfr>"
            + "			<result>0</result>"
            + "			<errCode/>"
            + "			<msg/>"
            + "			<orderedOfferingSets>"
            + "				<orderedOfferingSet>"
            + "					<offeringId>1068579845</offeringId>"
            + "					<OfferingName>&#25105;&#30340;e&#23478;_e9_200906</OfferingName>"
            + "					<offeringProductId>55116</offeringProductId>"
            + "					<orderDate>2010-01-07</orderDate>"
            + "					<isCourses>1</isCourses>"
            + "					<effectDate>2010-02-01</effectDate>"
            + "					<expireDate>2060-01-01</expireDate>"
            + "					<pricingPlanSets/>"
            + "					<prodSets>"
            + "						<prodSet>"
            + "							<productId>1068579741</productId>"
            + "							<productName>LAN&#32456;&#31471;</productName>"
            + "							<offeringProductId>481</offeringProductId>"
            + "							<effectDate>2010-01-20</effectDate>"
            + "							<expireDate>2099-01-01</expireDate>"
            + "							<orderDate>2010-01-07</orderDate>"
            + "							<serviceNbr>18056080891</serviceNbr>"
            + "							<isPrepay>0</isPrepay>"
            + "							<prodAttrSets>"
            + "								<prodAttrSet>"
            + "									<attrCode>4</attrCode>"
            + "									<attrName>&#20135;&#21697;&#23494;&#30721;</attrName>"
            + "									<attrValue>123123</attrValue>"
            + "								</prodAttrSet>"
            + "								<prodAttrSet>"
            + "									<attrCode>115</attrCode>"
            + "									<attrName>LAN&#25509;&#20837;&#36895;&#29575;</attrName>"
            + "									<attrValue>113</attrValue>"
            + "								</prodAttrSet>"
            + "								<prodAttrSet>"
            + "									<attrCode>314</attrCode>"
            + "									<attrName>LAN&#25509;&#20837;&#26041;&#24335;</attrName>"
            + "									<attrValue>520</attrValue>"
            + "								</prodAttrSet>"
            + "								<prodAttrSet>"
            + "									<attrCode>315</attrCode>"
            + "									<attrName>LAN&#35745;&#36153;&#26041;&#24335;</attrName>"
            + "									<attrValue>524</attrValue>"
            + "								</prodAttrSet>"
            + "								<prodAttrSet>"
            + "									<attrCode>317</attrCode>"
            + "									<attrName>&#25320;&#21495;&#24080;&#21495;</attrName>"
            + "									<attrValue>hfb66954</attrValue>"
            + "								</prodAttrSet>"
            + "								<prodAttrSet>"
            + "									<attrCode>318</attrCode>"
            + "									<attrName>LAN&#25320;&#21495;&#23494;&#30721;</attrName>"
            + "									<attrValue>491678</attrValue>"
            + "								</prodAttrSet>"
            + "								<prodAttrSet>"
            + "									<attrCode>319</attrCode>"
            + "									<attrName>LAN&#32465;&#23450;&#31867;&#22411;</attrName>"
            + "									<attrValue>9001</attrValue>"
            + "								</prodAttrSet>"
            + "								<prodAttrSet>"
            + "									<attrCode>320</attrCode>"
            + "									<attrName>LAN&#24320;&#25143;&#33410;&#28857;</attrName>"
            + "									<attrValue>318657</attrValue>"
            + "								</prodAttrSet>"
            + "								<prodAttrSet>"
            + "									<attrCode>601</attrCode>"
            + "									<attrName>&#32456;&#31471;&#25320;&#21495;&#24182;&#21457;&#25968;</attrName>"
            + "									<attrValue>1141</attrValue>"
            + "								</prodAttrSet>"
            + "								<prodAttrSet>"
            + "									<attrCode>611</attrCode>"
            + "									<attrName>&#34892;&#19994;&#24212;&#29992;&#19987;&#32447;</attrName>"
            + "									<attrValue>803944</attrValue>"
            + "								</prodAttrSet>"
            + "								<prodAttrSet>"
            + "									<attrCode>629</attrCode>"
            + "									<attrName>&#23485;&#24102;&#25509;&#20837;&#29992;&#36884;</attrName>"
            + "									<attrValue>1145</attrValue>"
            + "								</prodAttrSet>"
            + "								<prodAttrSet>"
            + "									<attrCode>632</attrCode>"
            + "									<attrName>&#26159;&#21542;&#36890;&#36807;&#23494;&#30721;&#39564;&#35777;</attrName>"
            + "									<attrValue>0</attrValue>"
            + "								</prodAttrSet>"
            + "								<prodAttrSet>"
            + "									<attrCode>656</attrCode>"
            + "									<attrName>&#22871;&#39184;&#23646;&#24615;</attrName>"
            + "									<attrValue>23</attrValue>"
            + "								</prodAttrSet>"
            + "								<prodAttrSet>"
            + "									<attrCode>698</attrCode>"
            + "									<attrName>&#35777;&#20214;&#31867;&#22411;</attrName>"
            + "									<attrValue>1</attrValue>"
            + "								</prodAttrSet>"
            + "								<prodAttrSet>"
            + "									<attrCode>699</attrCode>"
            + "									<attrName>&#35777;&#20214;&#21495;&#30721;</attrName>"
            + "									<attrValue>34012319820414364X</attrValue>"
            + "								</prodAttrSet>"
            + "								<prodAttrSet>"
            + "									<attrCode>701</attrCode>"
            + "									<attrName>&#23616;&#31449;&#32534;&#30721;</attrName>"
            + "									<attrValue>22763</attrValue>"
            + "								</prodAttrSet>"
            + "								<prodAttrSet>"
            + "									<attrCode>702</attrCode>"
            + "									<attrName>&#23616;&#31449;&#30340;&#21517;&#31216;</attrName>"
            + "									<attrValue>&#22269;&#36713;&#21517;&#33489;IAD&#23616;</attrValue>"
            + "								</prodAttrSet>"
            + "								<prodAttrSet>"
            + "									<attrCode>707</attrCode>"
            + "									<attrName>&#36873;&#22336;&#23616;&#21521;&#32534;&#30721;</attrName>"
            + "									<attrValue>5310</attrValue>"
            + "								</prodAttrSet>"
            + "								<prodAttrSet>"
            + "									<attrCode>708</attrCode>"
            + "									<attrName>&#36873;&#22336;&#23616;&#21521;&#21517;&#31216;</attrName>"
            + "									<attrValue>&#31185;&#25216;&#22253;&#23616;</attrValue>"
            + "								</prodAttrSet>"
            + "							</prodAttrSets>"
            + "						</prodSet>		"
            + "					</prodSets>"
            + "				</orderedOfferingSet>"
            + "			</orderedOfferingSets>"
            + "		</QueryCustOfr>"
            + "	</contractBody>"
            + "</ESB>";
    public String orderResponse = "<?xml version=\"1.0\" encoding=\"GB18030\"?>"
            + "<ESB>"
            + "<MSG_ID>1289723777173</MSG_ID>"
            + "<SERVICE_CD>exeBuss00003</SERVICE_CD>"
            + "<FROM>CRM</FROM>"
            + "<TO>WSS</TO>"
            + "<TIME>20101114163619</TIME>"
            + "<RETURN_CD>0</RETURN_CD>"
            + "<RETURN_DESC>操作成功</RETURN_DESC>"
            + "<contractBody>"
            + "<OfferAccept>"
            + "<result>0</result>"
            + "<errCode></errCode>"
            + "<msg>成功</msg>"
            + "<orderId>151472278</orderId>"
            + "</OfferAccept>"
            + "</contractBody>"
            + "</ESB>";
}
