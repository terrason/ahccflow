package cn.etuo.ahccflow.ctmp.qryflowservice;

public class GetRatableQryPortTypeProxy implements cn.etuo.ahccflow.ctmp.qryflowservice.GetRatableQryPortType {
  private String _endpoint = null;
  private cn.etuo.ahccflow.ctmp.qryflowservice.GetRatableQryPortType getRatableQryPortType = null;
  
  public GetRatableQryPortTypeProxy() {
    _initGetRatableQryPortTypeProxy();
  }
  
  public GetRatableQryPortTypeProxy(String endpoint) {
    _endpoint = endpoint;
    _initGetRatableQryPortTypeProxy();
  }
  
  private void _initGetRatableQryPortTypeProxy() {
    try {
      getRatableQryPortType = (new cn.etuo.ahccflow.ctmp.qryflowservice.GetRatableQry_ServiceLocator()).getgetRatableQryHttpPort();
      if (getRatableQryPortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)getRatableQryPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)getRatableQryPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (getRatableQryPortType != null)
      ((javax.xml.rpc.Stub)getRatableQryPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public cn.etuo.ahccflow.ctmp.qryflowservice.GetRatableQryPortType getGetRatableQryPortType() {
    if (getRatableQryPortType == null)
      _initGetRatableQryPortTypeProxy();
    return getRatableQryPortType;
  }
  
  public cn.etuo.ahccflow.ctmp.qryflowservice.String2StringMapEntry[] getRatableQry(cn.etuo.ahccflow.ctmp.qryflowservice.String2StringMapEntry[] in0) throws java.rmi.RemoteException{
    if (getRatableQryPortType == null)
      _initGetRatableQryPortTypeProxy();
    return getRatableQryPortType.getRatableQry(in0);
  }
  
  
}