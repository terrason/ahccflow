/***********************************************************************
 * @Project: ShareWeb
 * @Title: ResponseResult.java
 * @Package: cn.etuo.share.interfaces.entity
 * @Description: TODO
 * @author: Administrator
 * @date: 2013-3-22 下午2:32:43
 * @Copyright: 2013 www.etuo.cn Inc. All rights reserved.
 * @version: V1.0  
 *======================================================================
 * CHANGE HISTORY LOG
 *----------------------------------------------------------------------
 * MOD. NO.|  DATE    | NAME           | REASON            | CHANGE REQ.
 *----------------------------------------------------------------------
 *         |          |                |                   |
 * DESCRIPTION:
 ***********************************************************************/

package cn.etuo.ahccflow.ctmp.crmservice;

/**
 * @ClassName: ResponseResult
 * @Description: 接口返回实体
 * @author: yutf
 * @date: 2013-3-22 下午2:32:43
 */

public class OrderResponse {
	private int code;//0-成功 1-失败
	private String desc;//描述;
	private String fail;//失败号码
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getFail() {
		return fail;
	}
	public void setFail(String fail) {
		if(this.fail == null){
			this.fail = fail;
		}else{
			this.fail += "||" + fail;
		}
	}
}
