package cn.etuo.ahccflow.ctmp.crmservice;

import java.util.List;


public class ProInfo implements java.io.Serializable{
	private int isPrepay = -1;//0-后付费 1-预付费
	private String productId;
	private String groupIds;
	private String offeringProductIds;
	
	private List<Group> groups;
	
	public String getProductId() {
		return productId;
	}
	
	public void setProductId(String productId) {
		this.productId = productId;
	}
	
	public String getOfferingProductIds() {
		return offeringProductIds;
	}
	
	public void setOfferingProductIds(String offeringProductIds) {
		if(this.offeringProductIds == null || "".equals(this.offeringProductIds))
			this.offeringProductIds = offeringProductIds;
		else
			this.offeringProductIds = this.offeringProductIds + "," + offeringProductIds;
	}
	
	public int getIsPrepay() {
		return isPrepay;
	}
	
	public void setIsPrepay(int isPrepay) {
		this.isPrepay = isPrepay;
	}
	
	public String getGroupIds() {
		return groupIds;
	}
	
	public void setGroupIds(String groupIds) {
		if(this.groupIds == null || "".equals(this.groupIds))
			this.groupIds = groupIds;
		else
			this.groupIds = this.groupIds + "," + groupIds;
	}
	
	public List<Group> getGroups() {
		return groups;
	}
	
	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}
}
