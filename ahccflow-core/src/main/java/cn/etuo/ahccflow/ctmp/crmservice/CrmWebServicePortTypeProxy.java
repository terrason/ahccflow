package cn.etuo.ahccflow.ctmp.crmservice;

public class CrmWebServicePortTypeProxy implements CrmWebServicePortType {

    private String _endpoint = null;
    private CrmWebServicePortType crmWebServicePortType = null;

    public CrmWebServicePortTypeProxy() {
        _initCrmWebServicePortTypeProxy();
    }

    public CrmWebServicePortTypeProxy(String endpoint) {
        _endpoint = endpoint;
        _initCrmWebServicePortTypeProxy();
    }

    private void _initCrmWebServicePortTypeProxy() {
        try {
            crmWebServicePortType = (new CrmWebServiceLocator()).getCrmWebServiceHttpPort();
            if (crmWebServicePortType != null) {
                if (_endpoint != null) {
                    ((javax.xml.rpc.Stub) crmWebServicePortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
                } else {
                    _endpoint = (String) ((javax.xml.rpc.Stub) crmWebServicePortType)._getProperty("javax.xml.rpc.service.endpoint.address");
                }
            }

        } catch (javax.xml.rpc.ServiceException serviceException) {
        }
    }

    public String getEndpoint() {
        return _endpoint;
    }

    public void setEndpoint(String endpoint) {
        _endpoint = endpoint;
        if (crmWebServicePortType != null) {
            ((javax.xml.rpc.Stub) crmWebServicePortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        }

    }

    public CrmWebServicePortType getCrmWebServicePortType() {
        if (crmWebServicePortType == null) {
            _initCrmWebServicePortTypeProxy();
        }
        return crmWebServicePortType;
    }

    public java.lang.String query(java.lang.String in0) throws java.rmi.RemoteException {
        if (crmWebServicePortType == null) {
            _initCrmWebServicePortTypeProxy();
        }
        return crmWebServicePortType.query(in0);
    }

    public java.lang.String callService(java.lang.String in0) throws java.rmi.RemoteException {
        if (crmWebServicePortType == null) {
            _initCrmWebServicePortTypeProxy();
        }
        return crmWebServicePortType.callService(in0);
    }
}