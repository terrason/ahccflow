package cn.etuo.ahccflow.console.controller;

public interface Searchable {

    public static final String KEY_CURRENT_LOOKUP = "lookup";

    /**
     * 从session中获取当前的{@code lookup}对象.
     * 若不存在则{@link #newLookup() 创建}一个，并保存到session.
     *
     * <p>
     * 该返回值 与 使用{@link #KEY_CURRENT_LOOKUP 键}直接从{@code session}中获取的对象相同。
     * 即{@code session}中以{@link #KEY_CURRENT_LOOKUP 键}保存的{@code lookup}始终是当前的。</p>
     */
    public <T> T currentLookup();

    /**
     * 创建一个默认{@code lookup}.
     */
    public <T> T newLookup();
}
