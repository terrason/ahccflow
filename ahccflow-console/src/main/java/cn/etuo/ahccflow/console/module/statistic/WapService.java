package cn.etuo.ahccflow.console.module.statistic;

import cn.etuo.ahccflow.console.mybatis.WapUserStatisticMapper;
import cn.etuo.ahccflow.console.mybatis.WapVisitorStatisticMapper;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WapService {

    /**
     * 统计月份的每日访问量.
     *
     * @param month 月份
     * @return 每日访问量数组(长度固定为31)
     */
    public Integer[] statisticVisitCount(Date month,Integer channel) {
	Date thisMonth = DateUtils.truncate(month, Calendar.MONTH);
	Date nextMonth = DateUtils.addMonths(thisMonth, 1);
	Calendar calendar = DateUtils.toCalendar(thisMonth);
	int firstDay = calendar.getMinimum(Calendar.DAY_OF_MONTH);
	int lastDay = calendar.getMaximum(Calendar.DAY_OF_MONTH);
	Integer[] stat=new Integer[lastDay-firstDay+1];
	List<DailyStatisticEntity> selectDailyCount = wapVisitorStatisticMapper.selectDailyCount(thisMonth, nextMonth,channel);
	for (int i = 0; i < selectDailyCount.size(); i++) {
	    DailyStatisticEntity entity = selectDailyCount.get(i);
	    stat[entity.getDay()-1]=entity.getCount();
	}
	
	for (int i = 0; i < stat.length; i++) {
	    Integer integer = stat[i];
	    if(integer==null)
		stat[i]=0;
	}
	return stat;
    }

    /**
     * 统计月份的每日用户量.
     *
     * @param month 月份
     * @return 每日用户量数组(长度固定为31)
     */
    public Integer[] statisticUserCount(Date month,Integer channel) {
	Date thisMonth = DateUtils.truncate(month, Calendar.MONTH);
	Date nextMonth = DateUtils.addMonths(thisMonth, 1);
	Calendar calendar = DateUtils.toCalendar(thisMonth);
	int firstDay = calendar.getMinimum(Calendar.DAY_OF_MONTH);
	int lastDay = calendar.getMaximum(Calendar.DAY_OF_MONTH);
	Integer[] stat=new Integer[lastDay-firstDay+1];
	List<DailyStatisticEntity> selectDailyCount = wapUserStatisticMapper.selectDailyCount(thisMonth, nextMonth,channel);
	for (int i = 0; i < selectDailyCount.size(); i++) {
	    DailyStatisticEntity entity = selectDailyCount.get(i);
	    stat[entity.getDay()-1]=entity.getCount();
	}
	
	for (int i = 0; i < stat.length; i++) {
	    Integer integer = stat[i];
	    if(integer==null)
		stat[i]=0;
	}
	return stat;
    }

    /**
     * 统计月份的用户量.
     *
     * @param month 月份
     * @return 当月用户量
     */
    public int statisticUserTotal(Date month,Integer channel) {
	Date thisMonth = DateUtils.truncate(month, Calendar.MONTH);
	Date nextMonth = DateUtils.addMonths(thisMonth, 1);
	return wapUserStatisticMapper.selectTotal(thisMonth, nextMonth,channel);
    }

    @Autowired
    private WapVisitorStatisticMapper wapVisitorStatisticMapper;
    @Autowired
    private WapUserStatisticMapper wapUserStatisticMapper;
}
