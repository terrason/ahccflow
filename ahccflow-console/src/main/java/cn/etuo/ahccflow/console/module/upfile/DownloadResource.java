package cn.etuo.ahccflow.console.module.upfile;

import java.io.Serializable;

public class DownloadResource implements Serializable{

    private String name;
    private String url;
    private long length;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }
}
