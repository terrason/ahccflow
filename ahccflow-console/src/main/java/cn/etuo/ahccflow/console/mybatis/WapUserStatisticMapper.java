package cn.etuo.ahccflow.console.mybatis;

import cn.etuo.ahccflow.console.module.statistic.DailyStatisticEntity;
import java.util.Date;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WapUserStatisticMapper {

    /**
     * 统计每日访问量.
     *
     * @param start 开始日期
     * @param end 结束日期
     * @param channel 渠道
     * @return 每天访问量
     */
    public List<DailyStatisticEntity> selectDailyCount(@Param("start") Date start, @Param("end") Date end,@Param("channel") Integer channel);

    /**
     * 统计总用户量.
     *
     * @param start 开始日期
     * @param end 结束日期
     * @param channel 渠道
     * @return 期间内的用户量
     */
    public int selectTotal(@Param("start") Date start, @Param("end") Date end,@Param("channel") Integer channel);
}
