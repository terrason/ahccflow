package cn.etuo.ahccflow.console.controller.statistic;

import java.util.Calendar;
import java.util.Date;
import org.apache.commons.lang3.time.DateUtils;

public class WapLookup {

    private Date month = DateUtils.truncate(new Date(), Calendar.MONTH);
    private Integer channel = 1;

    public Date getMonth() {
	return month;
    }

    public void setMonth(Date month) {
	this.month = month;
    }

    public Integer getChannel() {
	return channel;
    }

    public void setChannel(Integer channel) {
	this.channel = channel;
    }

}
