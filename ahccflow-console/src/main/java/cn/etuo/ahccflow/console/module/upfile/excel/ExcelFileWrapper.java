/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cn.etuo.ahccflow.console.module.upfile.excel;

import cn.etuo.ahccflow.console.module.upfile.DownloadResource;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Value;


public class ExcelFileWrapper {
    @Value("#{settings['file.upload']}")
    private String rootDir;
    @Value("#{settings['file.download']}")
    private String rootPath;
    
    //TODO.
    private String downloadDir;
    private String downloadUrl;
    

    
    
    public DownloadResource toResource(ExcelSerializable serializable) throws IOException {
	String name=serializable.getName();
        //Step 1. Create a template file. Setup sheets and workbook-level objects such as cell styles, number formats, etc.
        XSSFWorkbook workbook = new XSSFWorkbook();
        Map<String,CellStyle> dateCellStyle = createDateCellStyle(workbook);

        XSSFSheet sheet = workbook.createSheet(name);
        //name of the zip entry holding sheet data, e.g. /xl/worksheets/sheet1.xml
        String sheetRef = sheet.getPackagePart().getPartName().getName();
        //save the template
        File template = writeToTempFile(workbook, name + "_template");

        //Step 2. Generate XML file.
        File xml = exportToXml(serializable,dateCellStyle, name + "_xml");

        //Step 3. Substitute the template entry with the generated data
        File xlsx = marge(template, xml, sheetRef.substring(1), name);

        DownloadResource resource = new DownloadResource();
        resource.setName(xlsx.getName());
        resource.setUrl(downloadUrl + xlsx.getName());
        resource.setLength(xlsx.length());
        return resource;
    }

    private File exportToXml(ExcelSerializable serializable, Map<String,CellStyle> styles,String name) throws IOException {
//        File tmp = File.createTempFile(name, ".xml");
//        final Writer fw = new OutputStreamWriter(new FileOutputStream(tmp), "UTF-8");
//        final SpreadSheetWriter sw = new SpreadSheetWriter(fw);
//        sw.beginSheet();
//        //insert header row
//        sw.insertRow(0);
//        sw.createCell(0, "手机号码");
//        sw.createCell(1, "流量包名称");
//        sw.createCell(2, "兑换时间");
//        sw.endRow();
//        //write data rows
//        StringBuilder builder = new StringBuilder();
//        final ArrayList<Object> params = new ArrayList<Object>();
//        builder.append("select u.mobile,flow.flow_name,opt.create_time from tbl_prize_").append(hcode).append(" opt left join tbl_client_user u on opt.user_id=u.id left join tbl_gift gift on opt.gift_id=gift.id left join tbl_flow flow on gift.gift_invent_id=flow.id where type=1");
//        if (lookup != null) {
//            if (StringUtils.hasText(lookup.getRangeBegin())) {
//                builder.append(" and opt.create_time >=?");
//                params.add(lookup.getRangeBegin());
//            }
//            if (StringUtils.hasText(lookup.getRangeEnd())) {
//                builder.append(" and opt.create_time <=?");
//                params.add(lookup.getRangeEnd());
//            }
//            if (StringUtils.hasText(lookup.getMobile())) {
//                builder.append(" and u.mobile=?");
//                params.add(lookup.getMobile());
//            }
//            if (StringUtils.hasText(lookup.getName())) {
//                builder.append(" and flow.flow_name like concat('%',?,'%')");
//                params.add(lookup.getName());
//            }
//        }
//        builder.append(" order by opt.create_time desc");
//        jdbcTemplate.execute(builder.toString(), new PreparedStatementCallback<Integer>() {
//
//            @Override
//            public Integer doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
//                for (int i = 0; i < params.size(); i++) {
//                    Object param = params.get(i);
//                    ps.setObject(i + 1, param);
//                }
//                ResultSet rs = ps.executeQuery();
//                int rownum = 0;
//                try {
//                    while (rs.next()) {
//                        sw.insertRow(++rownum);
//                        sw.createCell(0, rs.getString("mobile"));
//                        sw.createCell(1, rs.getString("flow_name"));
//                        Calendar createTime = Calendar.getInstance();
//                        createTime.setTime(rs.getTimestamp("create_time"));
//                        sw.createCell(2, createTime, dateCellStyle.getIndex());
//                        sw.endRow();
//                    }
//                } catch (IOException ex) {
//                    logger.error("无法写入文件", ex);
//                }
//                return rownum;
//            }
//        });
//        sw.endSheet();
//        fw.close();
//        return tmp;
	//TODO.
	throw new UnsupportedOperationException();
    }

    private File marge(File template, File xml, String sheetRef, String name) throws IOException {
        File resource = File.createTempFile(name + "_", ".xlsx", new File(rootDir + downloadDir));
        FileOutputStream out = new FileOutputStream(resource);
        ZipFile zip = new ZipFile(template);

        ZipOutputStream zos = new ZipOutputStream(out);

        Enumeration<ZipEntry> en = (Enumeration<ZipEntry>) zip.entries();
        while (en.hasMoreElements()) {
            ZipEntry ze = en.nextElement();
            if (!ze.getName().equals(sheetRef)) {
                zos.putNextEntry(new ZipEntry(ze.getName()));
                InputStream is = zip.getInputStream(ze);
                copyStream(is, zos);
                is.close();
            }
        }
        zos.putNextEntry(new ZipEntry(sheetRef));
        InputStream is = new FileInputStream(xml);
        copyStream(is, zos);
        is.close();

        zos.close();
        return resource;
    }

    private void copyStream(InputStream in, OutputStream out) throws IOException {
        byte[] chunk = new byte[1024];
        int count;
        while ((count = in.read(chunk)) >= 0) {
            out.write(chunk, 0, count);
        }
    }

    private File writeToTempFile(Workbook workbook, String name) throws IOException {
        FileOutputStream fos = null;
        try {
            File file = File.createTempFile(name + "_", ".xlsx");
            fos = new FileOutputStream(file);
            workbook.write(fos);
            return file;
        } finally {
            if (fos != null) {
                fos.close();
            }
        }
    }

    private Map<String,CellStyle> createDateCellStyle(Workbook workbook) {
	Map<String,CellStyle> styles=new HashMap<String, CellStyle>();
        CreationHelper createHelper = workbook.getCreationHelper();
        CellStyle dateCellStyle = workbook.createCellStyle();
        short format = createHelper.createDataFormat().getFormat("yyyy-mm-dd hh:mm:ss");
        dateCellStyle.setDataFormat(format);
	styles.put("datetime", dateCellStyle);
	
        return styles;
    }

}
