package cn.etuo.ahccflow.console.controller;

import cn.etuo.ahccflow.console.module.pager.Pager;

public interface Paginable {

    public static final String KEY_CURRENT_PAGER = "pager";

    /**
     * 从session中获取当前的{@code pager}对象. 若不存在则{@link #newPager() 创建}一个，并保存到session.
     *
     * <p>
     * 该返回值 与 使用{@link #KEY_CURRENT_PAGER 键}直接从{@code session}中获取的对象相同。
     * 即{@code session}中以{@link #KEY_CURRENT_PAGER 键}保存的{@code pager}始终是当前的。</p>
     */
    public Pager currentPager();

    /**
     * 创建一个默认pager.
     */
    public Pager newPager();
}
