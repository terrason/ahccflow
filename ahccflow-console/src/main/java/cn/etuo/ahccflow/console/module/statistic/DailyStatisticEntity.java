package cn.etuo.ahccflow.console.module.statistic;

import java.io.Serializable;

public class DailyStatisticEntity implements Serializable {

    private int day;
    private int count;

    public int getDay() {
	return day;
    }

    public void setDay(int day) {
	this.day = day;
    }


    public int getCount() {
	return count;
    }

    public void setCount(int count) {
	this.count = count;
    }
}
