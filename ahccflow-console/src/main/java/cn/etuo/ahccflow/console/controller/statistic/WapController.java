package cn.etuo.ahccflow.console.controller.statistic;

import cn.etuo.ahccflow.console.controller.SapControllerAdaptor;
import cn.etuo.ahccflow.console.module.statistic.WapService;
import java.util.Calendar;
import java.util.Date;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping
public class WapController extends SapControllerAdaptor {

    @RequestMapping(value = "/statistic/wap", method = RequestMethod.GET)
    public String home(Model model) {
	WapLookup lookup = currentLookup();
	Integer[] visitors = service.statisticVisitCount(lookup.getMonth(), lookup.getChannel());
	Integer[] users = service.statisticUserCount(lookup.getMonth(), lookup.getChannel());
	int total = service.statisticUserTotal(lookup.getMonth(), lookup.getChannel());
	model.addAttribute("visitors", visitors);
	model.addAttribute("users", users);
	model.addAttribute("usersThisMonth", total);

	Calendar now = DateUtils.toCalendar(lookup.getMonth());
	int size = now.getActualMaximum(Calendar.DAY_OF_MONTH);
	int month = now.get(Calendar.MONTH) + 1;
	model.addAttribute("size", size);
	model.addAttribute("month", month);
	return "/statistic/wap/home";
    }

    @RequestMapping(value = "/statistic/wap/", method = RequestMethod.POST)
    public String search(@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM") Date time,
	    @RequestParam(required = false) Integer channel) {
	if (time != null) {
	    WapLookup lookup = currentLookup();
	    lookup.setMonth(time);
	}
	if (channel != null) {
	    WapLookup lookup = currentLookup();
	    lookup.setChannel(channel);
	}
	return "redirect:/statistic/wap";
    }

    @Override
    public Object newLookup() {
	return new WapLookup();
    }
    @Autowired
    private WapService service;
}
