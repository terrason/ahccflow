/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.etuo.ahccflow.console.module.upfile.excel;

import java.util.Map;
import org.apache.poi.ss.usermodel.CellStyle;

public interface ExcelSerializable {
    public String getName();
    public void serialize(SpreadSheetWriter writer, Map<String, CellStyle> cellStyles);
}
