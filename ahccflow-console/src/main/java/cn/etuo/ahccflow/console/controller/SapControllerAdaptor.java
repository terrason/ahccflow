package cn.etuo.ahccflow.console.controller;

import cn.etuo.ahccflow.console.module.pager.DefaultPager;
import cn.etuo.ahccflow.console.module.pager.Pager;
import java.util.HashMap;
import javax.servlet.http.HttpSession;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class SapControllerAdaptor implements Searchable, Paginable {

    /**
     * 返回分组标识. 同一组的Controller共享{@code lookup}和{@code pager}.
     *
     * @return
     */
    protected String getGroup() {
	return getClass().getName();
    }

    @Override
    public <T> T currentLookup() {
	HttpSession session = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getSession();
	Object lookup = session.getAttribute(getLookupKey());
	if (lookup == null) {
	    lookup = newLookup();
	    session.setAttribute(getLookupKey(), lookup);
	}
	session.setAttribute(KEY_CURRENT_LOOKUP, lookup);
	return (T) lookup;
    }

    /**
     * {@inheritDoc }
     *
     * <p>
     * 创建一个{@code HashMap<String,String>} 作为默认{@code lookup}. 子类建议覆盖此方法。</p>
     */
    @Override
    public Object newLookup() {
	return new HashMap<String, String>();
    }

    @Override
    public Pager currentPager() {
	HttpSession session = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getSession();
	Pager pager = (Pager) session.getAttribute(getLookupKey());
	if (pager == null) {
	    pager = newPager();
	    session.setAttribute(getPagerKey(), pager);
	}
	session.setAttribute(KEY_CURRENT_PAGER, pager);
	return pager;
    }

    /**
     * {@inheritDoc }
     *
     * <p>
     * 创建一个{@code HashMap<String,String>} 作为默认{@code lookup}. 子类建议覆盖此方法。</p>
     */
    @Override
    public Pager newPager() {
	return new DefaultPager();
    }

    private String lookupKey;
    private String pagerKey;

    private String getLookupKey() {
	if (lookupKey == null) {
	    lookupKey = getGroup() + "." + KEY_CURRENT_LOOKUP;
	}
	return lookupKey;
    }

    private String getPagerKey() {
	if (pagerKey == null) {
	    pagerKey = getGroup() + "." + KEY_CURRENT_PAGER;
	}
	return pagerKey;
    }
}
