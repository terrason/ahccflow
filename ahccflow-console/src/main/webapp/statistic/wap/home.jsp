<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width; initial-scale=1.0;minimum-scale=1.0; maximum-scale=2.0;user-scalable=yes" />
        <title><spring:message code="application.title"/></title>
        <%@include file="/WEB-INF/jspf/common.jspf" %>
    </head>
    <body>
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="javascript:;">首页</a></li>
                <li><a href="javascript:;">统计</a></li>
                <li class="active">WAP页面访问统计</li>
            </ol>
        </div>
        <div class="container">
            <form class="form-inline lookup clearfix" role="form" action="" method="post">
                <div class="form-group">
                    <label class="sr-only" for="lookup-time">选择统计月份</label>
                    <input id="lookup-time" class="form-control" type="datetime" name="time"
			   placeholder="选择统计月份"
			   data-format="yyyy-mm"
			   data-start-view="3"
			   data-min-view="3"
			   value="<fmt:formatDate value="${lookup.month}" pattern="yyyy-MM"/>"/>
                    <span class="add-on"><i class="icon-th"></i></span>
                </div>
                <div class="form-group">
                    <label class="sr-only" for="lookup-channel">选择渠道</label>
		    <select id="lookup-channel" class="form-control" name="channel" data-value="${lookup.channel}">
			<option value="1">腾讯管家</option>
			<option value="2">百度</option>
			<option value="3">智慧安徽</option>
		    </select>
                </div>
                <div class="form-group">
                    <button type="submit" class="form-control btn btn-primary">查询</button>
                </div>
                <div class="form-group">
                    <button type="button" class="form-control btn btn-info reset">重置</button>
                </div>
            </form>
        </div>
        <div class="container"><hr/></div>
        <div class="container action-bar sr-only">
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#downloadDialog"
                    data-href="export">导出</button>

        </div>
        <div class="container">
	    <table class="table table-hover statistical">
                <thead>
                    <tr>
                        <th>${month}月</th>
                        <th>访问量</th>
                        <th>用户量</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="statistical-column-total">
                        <td class="statistical-ignore">总计</td>
			<td></td>
                        <td class="statistical-ignore" title="当月用户量">${usersThisMonth}</td>
                    </tr>
                    <c:forEach begin="0" end="${size-1}" varStatus="cur">
                        <tr class="statistical-data">
                            <td class="statistical-ignore">${cur.count}日</td>
			    <td>${visitors[cur.index]}</td>
			    <td>${users[cur.index]}</td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
        <%@include file="/WEB-INF/jspf/download.jspf" %>
    </body>
</html>
