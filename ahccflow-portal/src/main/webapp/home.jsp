<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width; initial-scale=1.0;minimum-scale=1.0; maximum-scale=2.0;user-scalable=yes" />
        <title><spring:message code="application.title"/></title>
        <link rel="stylesheet" type="text/css" href="${ctx}/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="${ctx}/css/common.css" />
        <script type="text/javascript" src="${ctx}/js/jquery-1.10.2.min.js"></script>
    </head>
    <body style="background-color: #ffffff;">
        <div id="list_all">
            <div id="banner">
                <img style="width: 100%;" src="${ctx}/img/tx_banner.${channel}.png" />
            </div>
            <div style="width: 90%; margin:5px auto;">
                <div class="link_lines">
                    <img src="${ctx}/img/list_18.png" style="width: 100%; float: left; height: 1px;" />
                </div>
                <div style="clear: both; height: 2px;"></div>
            </div>
            <div style="width: 90%; margin: 0px auto;">
                <p class="description">手机上网流量包：不限时，更优惠，轻松实现随时随地高速上网。此产品可重复订购，超级大流量想怎么用都行！</p>
                <c:forEach items="${flowList}" var="cur">
                    <div class="list_1">
                        <div class="list_info">
                            <div>
                                <div class="logo">
                                    <img src="${ctx}/img/b1.png" />
                                </div>
                                <div class="title">
                                    <div class="yx">
                                        <fmt:formatNumber minFractionDigits="0" value="${cur.price}"/>元${cur.name}
                                        <c:if test="${cur.hot}"><img alt="hot" src="${ctx}/img/hot.gif"/></c:if>
					</div>
				    </div>
				</div>
				<div class="count">
				    <div style="height:26px; " id="${cur.id}" class="orderBtn ${cur.enabled ? 'enabled':'disabled'}" data-prepaid="${cur.prepaid}">
                                    <img src="${ctx}/img/dg.png" style="border: 0;" />
                                </div>
                            </div>
                            <div style="clear: both; height: 5px;"></div>

                        </div>
                        <div class="link_line">
                            <img src="${ctx}/img/list_18.png" style="width: 100%; float: left; height: 1px;" />
                        </div>
                    </div>
                </c:forEach>

                <p class="description">流量加餐包：订购立即生效，月底失效，随时随地给手机加油。产品支持重复叠加订购，流量马上跑起来！</p>
                <c:forEach items="${additionalFlowList}" var="cur">
                    <div class="list_1">
                        <div class="list_info">
                            <div>
                                <div class="logo">
                                    <img src="${ctx}/img/b1.png" />
                                </div>
                                <div class="title">
                                    <div class="yx">
					<fmt:formatNumber minFractionDigits="0" value="${cur.price}"/>元${cur.name}
                                        <c:if test="${cur.hot}"><img alt="hot" src="${ctx}/img/hot.gif"/></c:if>
					</div>
				    </div>
				</div>
				<div class="count">
				    <div style="height:26px; " id="${cur.id}" class="orderBtn ${cur.enabled ? 'enabled':'disabled'}" data-prepaid="${cur.prepaid}">
                                    <img src="${ctx}/img/dg.png" style="border: 0;"/>
                                </div>
                            </div>
                            <div style="clear: both; height: 5px;"></div>

                        </div>
                        <div class="link_line">
                            <img src="${ctx}/img/list_18.png"
                                 style="width: 100%; float: left; height: 1px;" />
                        </div>
                    </div>
                </c:forEach>

                <p class="description">流量闲时包：订购立即生效，每月自动续订。仅限省内使用，闲时时段：<code>23：00——7：00</code></p>
                <c:forEach items="${easyFlowList}" var="cur">
                    <div class="list_1">
                        <div class="list_info">
                            <div>
                                <div class="logo">
                                    <img src="${ctx}/img/b1.png" />
                                </div>
                                <div class="title">
                                    <div class="yx">
					<fmt:formatNumber minFractionDigits="0" value="${cur.price}"/>元${cur.name}
                                        <c:if test="${cur.hot}"><img alt="hot" src="${ctx}/img/hot.gif"/></c:if>
					</div>
				    </div>
				</div>
				<div class="count">
				    <div style="height:26px; " id="${cur.id}" class="orderBtn ${cur.enabled ? 'enabled':'disabled'}" data-prepaid="${cur.prepaid}">
                                    <img src="${ctx}/img/dg.png" style="border: 0;" />
                                </div>
                            </div>
                            <div style="clear: both; height: 5px;"></div>

                        </div>
                        <div class="link_line">
                            <img src="${ctx}/img/list_18.png"
                                 style="width: 100%; float: left; height: 1px;" />
                        </div>
                    </div>
                </c:forEach>
            </div>
            <div id="list_foot">
                <div>皖ICP备05019141号-2 Copyright</div>
                <div>2013 安徽电信.All rights reserved.</div>
            </div>
            <br />
        </div>
        <script type="text/javascript">
	    $(".orderBtn").click(function() {
		var enabled = $(this).hasClass("enabled");
		var prepaid = $(this).data("prepaid");
		if (!enabled) {
		    window.alert(prepaid ? "您是预付费用户，无法订购后付费流量包！" : "您是后付费用户，无法订购预付费流量包！");
		} else {
		    window.location = "${ctx}/${channel}/" + this.id;
		}
	    });
        </script>
    </body>
</html>
