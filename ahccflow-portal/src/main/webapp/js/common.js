(function(window, $) {
    var $application = function(context) {
        if ($.onApplicationStart && $.isFunction($.onApplicationStart)) {
            $.onApplicationStart();
        }
        for (var name in $application) {
            var app = $application[name];
            if (app.enable) {
                app.init(context);
            }
        }

    };
    $application.statistical = {
        enable: true,
        selector: {
            context: ".statistical",
            columnTotal: "tr.statistical-column-total td:not(.statistical-ignore)",
            columnData: "tr.statistical-data td:nth-child({})",
            rowRangeTotal: "td.statistical-row-total",
            rowRangeData: "td:not(.statistical-ignore)"
        },
        event: {
            reload: "statistic.reload"
        },
        init: function(context) {
            var module = this;
            $(module.selector.context, context).each(function(i, statisticalContxt) {
                var $rowRangeTotal = $(module.selector.rowRangeTotal, statisticalContxt);
                $rowRangeTotal.on(module.event.reload, function() {
                    var $cell = $(this);
                    var column = $cell.prevAll().length;
                    var range = $cell.data("range");
                    if (!range) {
                        range = [0, column - 1];
                    }
                    if (!$.isArray(range)) {
                        range = [range, column - 1];
                    } else if (range.length === 1) {
                        range.push(column - 1);
                    }
                    var total = 0;
                    $(module.selector.rowRangeData, $cell.parent()).slice(range[0], range[1]).each(function() {
                        var v = parseFloat(this.innerHTML);
                        total += isNaN(v) ? 0 : v;
                    });
                    this.innerHTML = total;
                }).trigger(module.event.reload);

                var $columnTotal = $(module.selector.columnTotal, statisticalContxt);
                $columnTotal.on(module.event.reload, function() {
                    var total = 0;
                    var $data = $(module.selector.columnData.replace(/\{\}/, $columnTotal.index(this) + 2), statisticalContxt);
                    $data.each(function() {
                        var v = parseFloat(this.innerHTML);
                        total += isNaN(v) ? 0 : v;
                    });
                    this.innerHTML = total;
                }).trigger(module.event.reload);
            });
        }
    };
    $application.pagination = {
        enable: $.fn.pager,
        selector: ".pager-list",
        init: function(context) {
            $(this.selector, context).pager();
        }
    };
    $application.datetimepicker = {
        enable: $.fn.datetimepicker,
        selector: "input[type=datetime]",
        init: function(context) {
            $(this.selector, context).each(function(i, dt) {
                var $input = $(dt);
                var option = $input.data();
                $input.datetimepicker(option);
            });
        }
    };
    $application.nailing = {
        enable: $.fn.nails,
        selector: ".nailing",
        init: function(context) {
            $(this.selector, context).each(function() {
                var $nailing = $(this);
                $nailing.nails($nailing.data());
            });
        }
    };
    $application.alternate = {
        enable: true,
        selector: ".tab1 tr:even,form.resource table tr:even",
        alterClass: "alt",
        init: function(context) {
            $(this.selector, context).addClass(this.alterClass);
        }
    };
    $application.hoverable = {
        enable: true,
        selector: ".hoverable,.tab1 tr",
        defaultHoverClass: "over",
        init: function(context) {
            var module = this;
            $(this.selector, context).hover(function() {
                $(this).addClass($(this).data("hoverClass") || module.defaultHoverClass);
            }, function() {
                $(this).removeClass($(this).data("hoverClass") || module.defaultHoverClass);
            });
        }
    };
    $application.sortable = {
        enable: $.fn.sortable,
        selector: ".sortable",
        option: {
            cursor: "move",
            placeholder: "list-group-item ui-state-highlight"
        },
        init: function(context) {
            var module = this;
            $(this.selector, context).each(function() {
                var $sortable = $(this);
                var opt = $.extend(module.option, $sortable.data());
                $sortable.sortable(opt);
            });
        }
    };
    $application.selectable = {
        enable: $.fn.selectable,
        selector: ".selectable",
        option: {},
        init: function(context) {
            $(this.selector, context).selectable();
        }
    };
    $application.moveable = {
        enable: false,
        selector: ".moveable",
        option: {
            target: "tr",
            move: "top"
        },
        init: function(context) {
            var opt = this.option;
            $(this.selector, context).click(function() {
                var $this = $(this);
                var option = $.extend({}, opt, $this.data());
                var $target = $this.parents(option.target);
                var $container = $target.parent();
                switch (option.move) {
                    case "top" :
                        $container.prepend($target);
                        break;
                    case "bottom":
                        $container.append($target);
                        break;
                }
            });
        }
    };
    $application.checkboxBonds = {
        enable: true,
        chkAll: "#chkAll",
        pks: "[name=pks]:checkbox:not(:disabled)",
        //pks: "[name=pks]:checkbox",
        render: ":button[data-confirm-require=pks]",
        init: function(context) {
            var $pks = $(this.pks, context);
            var $chkAll = $(this.chkAll, context);
            var $render = $(this.render, context);
            $pks.click(function() {
                $chkAll.prop("checked", $pks.length === $pks.filter(":checked").length);
                $render.prop("disabled", $pks.filter(":checked").length === 0);
            });
            $chkAll.click(function() {
                $pks.prop("checked", $(this).prop("checked"));
                $render.prop("disabled", $pks.filter(":checked").length === 0);
                $pks.change();
            });
            $render.prop("disabled", $pks.filter(":checked").length === 0);
        }
    };
    $application.validation = {
        enable: $.fn.validate,
        selector: "form.validate",
        init: function(context) {
            var $form = $(this.selector, context);
            var option = $.extend({
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }, submitHandler: function(form) {
                    $form.hasClass("ajax") ? $(form).ajaxSubmit() : form.submit();
                }
            }, $form.data());
            $form.validate(option);
        }
    };
    $application.selectValueInitialization = {
        enable: true,
        selector: "select",
        event: "application.select.initialize",
        init: function(context) {
            $(this.selector, context).on(this.event, function(event) {
                var $select = $(this);
                var value = $select.data("value");
                if (value !== undefined) {
                    $select.children("option:selected").prop("selected", false);
                    $select.children("option[value=" + value + "]").prop("selected", true);
                }
                var autofire = $select.data("autofire");
                if (autofire) {
                    $select.change();
                }
            }).trigger(this.event);
        }
    };
    $application.trimText = {
        enable: true,
        selector: ":text:not(.notrim)",
        init: function(context) {
            $(this.selector, context).change(function() {
                this.value = $.trim(this.value);
            });
        }
    };
    $application.resetLookup = {
        enable: true,
        selector: {
            reset: ".reset",
            control: ":input",
            form: "form.lookup"
        },
        init: function(context) {
            var $lookupForm = $(this.selector.form, context);
            var controlSelector = this.selector.control;
            $(this.selector.reset, $lookupForm).click(function() {
                $(controlSelector, $lookupForm).val("");
                $lookupForm.submit();
            });
        }
    };
    $application.action = {
        enable: true,
        init: function(context) {
            for (var name in this) {
                var module = this[name];
                if (module && module.init) {
                    module.init(context);
                }
            }
        },
        addModule: function(name, module) {
            if (module && module.init) {
                this[name] = module;
            }
        },
        removeModule: function(name) {
            this[name] = undefined;
        },
        get: {
            selector: ".action-get",
            init: function(context) {
                $(this.selector, context).click(function() {
                    var $button = $(this);
                    var confirmRequire = $button.data("confirmRequire");
                    if (confirmRequire) {
                        var $requiremence = $(confirmRequire, context);
                        if ($requiremence.length === 0) {
                            window.alert($button.data("confirmRequireMessage") || "未满足操作条件");
                            return;
                        }
                    }
                    var confirm = $button.data("javascriptConfirm");
                    if (confirm) {
                        if (typeof (confirm) === "boolean") {
                            confirm = "您确定要执行此项操作吗？";
                        }
                        if (!window.confirm(confirm)) {
                            return;
                        }
                    }
                    window.location = $button.data("href") || $button.data("get") + ".action?" + $.param($button.data());
                });
            }
        },
        post: {
            selector: ".action-post",
            init: function(context) {
                $(this.selector, context).click(function() {
                    var $this = $(this);
                    var confirmRequire = $this.data("confirmRequire");
                    if (confirmRequire) {
                        var $requiremence = $(confirmRequire, context);
                        if ($requiremence.length === 0) {
                            window.alert($this.data("confirmRequireMessage") || "未满足操作条件");
                            return;
                        }
                    }
                    var confirm = $this.data("javascriptConfirm");
                    if (confirm) {
                        if (typeof (confirm) === "boolean") {
                            confirm = "您确定要执行此项操作吗？";
                        }
                        if (!window.confirm(confirm)) {
                            return;
                        }
                    }

                    var $form = $this.parents("form");
                    var formSelector = $this.data("form");
                    if (formSelector) {
                        $form = $(formSelector, context);
                    }
                    if (!$form.length) {
                        $form = $('<form method="post"></form>');
                        $(context).parents("body").append($form);
                    }
                    var post = $this.data("post") || $this.data("href");
                    if (post) {
                        $form.attr("action", post);
                    }
                    var data = $this.data();
                    $.each(data, function(key, value) {
                        if (value) {
                            var $input = $("<input type=\"hidden\" class=\"help\"/>");
                            $form.append($input);
                            $input.attr("name", key);
                            $input.val(value);
                        }
                    });
                    $form.submit();
                    $(".help:hidden", $form).remove();
                });
            }
        },
        /**
         * @deprecated Use $application.dialog instead.
         */
        dialog: {
            selector: ".action-dialog",
            init: function(context) {
                $(this.selector, context).click(function() {
                    var $actionButton = $(this);
                    var data = $actionButton.data();
                    var option = $.extend({
                        lock: true,
                        okValue: "保存",
                        ok: function() {
                            var $form = $("form", this.dom.content);
                            $form.find("button[type=submit]").click();
                            return false;
                        },
                        cancelValue: "取消",
                        cancel: function() {
                            return true;
                        }
                    }, data);
                    if (data.readonly) {
                        option.ok = undefined;
                    }
                    var dialog = $.dialog(option);
                    $.ajax({
                        url: data.url,
                        dataType: "html",
                        cache: false,
                        data: data
                    }).done(function(data, textStatus, jqXHR) {
                        var $dialog = $(data);
                        dialog.content($dialog[0]);
                        var $form = $dialog.is("form") ? $dialog : $("form", $dialog);
                        $form.attr("action", option.dialogUrl || option.url);
                        $application(dialog.dom.content);
                    });
                });
            }
        }
    };
    $application.dialog = {
        enable: $.dialog,
        selector: ".dialog",
        event: {
            remoteContentReady: "application.dialog.remote.ready"
        },
        ok: function() {
            var $form = $("form", this.dom.content);
            $form.find("button[type=submit]").click();
            return false;
        },
        cancel: function() {
            return true;
        },
        ready: function() {
            $application(this.dom.content);
        },
        init: function(context) {
            var thisModule = this;
            $(thisModule.selector, context).click(function() {
                var $actionButton = $(this);
                var data = $actionButton.data();
                var option = $.extend({
                    lock: true,
                    okValue: "保存",
                    ok: thisModule.ok,
                    cancelValue: "取消",
                    cancel: thisModule.cancel
                }, data);
                if (data.readonly) {
                    option.ok = undefined;
                }
                var dialog = $.dialog(option);
                $.ajax({
                    url: data.url,
                    dataType: "html",
                    cache: false,
                    data: data
                }).done(function(data, textStatus, jqXHR) {
                    var $dialog = $(data);
                    dialog.content($dialog[0]);
                    var $form = $dialog.is("form") ? $dialog : $("form", $dialog);
                    $form.attr("action", option.dialogUrl || option.url);
                    thisModule.ready.call(dialog);
                    $actionButton.trigger(thisModule.event.remoteContentReady, {module: thisModule, dialog: dialog, $content: $dialog, $form: $form});
                });
            });
        }
    };

    function _replaceMetacharator(metastring) {
        return metastring.replace(/[.]/, "\\.");
    }
    //Copy from jQuery.
    if (typeof module === "object" && module && typeof module.exports === "object") {
        module.exports = $application;
    } else {
        window.$application = $application;
        if (typeof define === "function" && define.amd) {
            define("$application", [], function() {
                return $application;
            });
        }
    }
})(window, jQuery);

$(document).ready(function() {
    window.$application(document);
});