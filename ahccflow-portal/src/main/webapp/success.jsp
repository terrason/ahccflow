<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width; initial-scale=1.0;minimum-scale=1.0; maximum-scale=2.0;user-scalable=yes" />
        <title><spring:message code="application.title"/>-订购成功</title>
        <link rel="stylesheet" type="text/css" href="${ctx}/css/bootstrap.min.css" />
    </head>
    <body>
	<div class="container">
	    <div class="row">
		<span class="col-xs-4 col-sm-5 text-right"><img alt="success" src="${ctx}/img/success.png"/></span>
		<span class="col-xs-8 col-sm-7 text-success" style="margin-top:44px;">
		    <p>您已成功订购${param.name}</p>
		    <p>谢谢使用!</p>
		</span>
	    </div>
	    <div class="row">
		<span class="col-xs-11 text-center text-info">
		    您还可以<a href="${ctx}/${channel}" >订购其他流量包</a>
		</span>
	    </div>
	</div>
    </body>
</html>
