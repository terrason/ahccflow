<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width; initial-scale=1.0;minimum-scale=1.0; maximum-scale=2.0;user-scalable=yes" />
        <title><spring:message code="application.title"/></title>
        <%@include file="/WEB-INF/jspf/common.jspf" %>
    </head>
    <body>
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="javascript:;">首页</a></li>
                <li><a href="javascript:;">统计</a></li>
                <li class="active">WAP 页面</li>
            </ol>
        </div>
        <div class="container">
            <form class="form-inline lookup clearfix" role="form" action="" method="post">
                <div class="form-group">
                    <label class="sr-only" for="lookup-time">选择统计月份</label>
                    <input type="datetime" class="form-control" id="lookup-time" placeholder="选择统计月份" name="time" data-format="yyyy-mm" data-autoclose="true" data-language="${lang}" value="${lookup.rangeBegin}"/>
                    <span class="add-on"><i class="icon-th"></i></span>
                </div>
                <div class="form-btn-bar">
                    <button type="submit" class="btn btn-primary">查询</button>
                    <button type="button" class="btn btn-info reset">重置</button>
                </div>
            </form>
        </div>
        <div class="container"><hr/></div>
        <div class="container action-bar">
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#downloadDialog"
                    data-href="export">导出</button>

        </div>
        <div class="container">
	    <table class="table table-hover statistical">
                <thead>
                    <tr>
                        <th><fmt:formatDate value="${now}" pattern="M月"/></th>
                        <th>WAP 访问量</th>
                        <th>WAP 用户量</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="statistical-column-total">
                        <td class="statistical-ignore">总计</td>
			<td></td>
                        <td></td>
                    </tr>
                    <c:forEach begin="1" end="31" varStatus="cur">
                        <tr class="statistical-data">
                            <td class="statistical-ignore">${cur.count}日</td>
			    <td>${visitorStatisticList[cur.index]}</td>
			    <td>${userStatisticList[cur.index]}</td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
	<%@include file="/WEB-INF/jspf/pager.jspf" %>
        <%@include file="/WEB-INF/jspf/download.jspf" %>
    </body>
</html>
