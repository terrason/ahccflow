<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width; initial-scale=1.0;minimum-scale=1.0; maximum-scale=2.0;user-scalable=yes" />
        <title><spring:message code="application.title"/>-流量包详情</title>
        <link rel="stylesheet" type="text/css" href="${ctx}/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="${ctx}/css/common.css" />
        <script type="text/javascript" src="${ctx}/js/jquery-1.10.2.min.js"></script>
	<script language="javascript">
	    function checkup() {
		return window.confirm("本月您只有一次订购机会,\n确定要订购吗?");
	    }
	</script>
    </head>

    <body>
	<form action="floworder" method="post" onsubmit="checkup">
	    <div class="total">
		<div class="d_middle">
		    <div class="info">
			<input type="hidden" name="id" value="${flow.id}" />
			<input type="hidden" name="name" value="${flow.name}" />
			<ul>
			    <li class="uli">${flow.name}</li>
			    <li class="uli_hr"><hr /></li>
			    <li class="uli">手机号码：${flow.mobile}</li>
			    <li class="uli_hr"><hr /></li>
			    <li class="uli">资费：${flow.price}</li>
			</ul>
		    </div>
		    <div class="ts">
                        <div class="ts_1">温馨提示：</div>
                        <div class="ts_1">1.订购流量包，立享超大流量，资费低至5分/M，可重复订购。</div>
                        <div class="ts_1">2.订单成功提交之后,系统将于48小时内短信通知您订购结果.</div>
                        <div class="ts_1">3.订购成功后,当月订购当月收取费用,流量立即生效.</div>
		    </div>
		    <br />
		</div>
		<br />
		<div class="foot">
		    <button type="submit" style="border:0px none;width:100%;padding:0px;"><img style="width: 100%;" src="${ctx}/img/qr.png"/></button>
		</div>
		<br />
	    </div>
	</form>
    </body>
</html>
