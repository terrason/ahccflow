<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>
<html>
    <head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width; initial-scale=1.0;minimum-scale=1.0; maximum-scale=2.0;user-scalable=yes" />
	<title><spring:message code="application.title"/>-订购失败</title> 
        <link rel="stylesheet" type="text/css" href="${ctx}/css/bootstrap.min.css" />
    </head>

    <body>
	<img src="${ctx}/img/error.png" class="img-responsive" style="width:100%;" alt="${exception.message}" title="${exception.message}"/>
    </body>
</html>