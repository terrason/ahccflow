<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width; initial-scale=1.0;minimum-scale=1.0; maximum-scale=2.0;user-scalable=yes" />
        <title>对不起，出错啦！</title>
        <link rel="stylesheet" type="text/css" href="${ctx}/css/bootstrap.min.css" />
    </head>
    <body>
	<div class="container">
	    <div class="page-header">
		<h3 class="text-warning">对不起，出错啦！<small>错误代码：${code}</small></h3>
		<p>您访问的页面出错，服务器返回错误信息：</p>
		<pre class="alert alert-danger">${exception.message}</pre>
		<a class="btn btn-info" href="${ctx}/${empty channel ? '' : channel}"><span class="glyphicon glyphicon-home"></span>返回首页</a>
	    </div>
	</div>
	<!--
	Failed URL: ${url}
	Exception:  ${exception.message}
	<c:forEach items="${exception.stackTrace}" var="cur">
	    ${cur}
	</c:forEach>
	-->
    </body>
</html>
