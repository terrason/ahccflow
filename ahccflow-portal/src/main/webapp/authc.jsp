<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width; initial-scale=1.0;minimum-scale=1.0; maximum-scale=2.0;user-scalable=yes" />
        <title><spring:message code="application.title"/>-登录认证</title>
        <link rel="stylesheet" type="text/css" href="${ctx}/css/bootstrap.min.css" />
        <script type="text/javascript" src="${ctx}/js/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="${ctx}/js/jquery-validation/jquery.validate.min.js"></script>
        <script type="text/javascript" src="${ctx}/js/jquery-validation/additional-methods.js"></script>
        <script type="text/javascript" src="${ctx}/js/jquery-validation/messages_zh.js"></script>
        <style type="text/css">
	    .input-group{width:100%;}
	    .form-group label{padding-right:0em;}
	    .form-group .help-block{text-align:right;}
	</style>
    </head>
    <body>
        <div class="container">
	    <div class="page-header">
		<h1>登录认证</h1>
	    </div>
        </div>
        <div class="container">
            <form class="container" id="authcForm" role="form" method="post" action="signIn" >
		<input type="hidden" name="flowview" value="${exception.flowview}" />
                <div class="form-group">
                    <label for="mobile" class="control-label">手机号码</label>
                    <div class="input-group">
                        <input type="tel" class="form-control" name="mobile" id="mobile" placeholder="请输入手机号码" data-rule-required="true" data-rule-mobile="true" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="smsAuthenticationCode" class="control-label">验证码</label>
                    <div class="input-group">
                        <input type="number" class="form-control" name="smsAuthenticationCode" id="smsAuthenticationCode" placeholder="手机短信验证码" data-rule-required="true"/>
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-info" id="smsBtn">获取验证码</button>
                        </span>
                    </div>
                </div>
                <div class="form-group">
		    <button type="submit" class="btn btn-primary">登录认证</button>
		    <button type="button" class="btn btn-default" onclick="window.history.back()">取消</button>
                </div>
            </form>
        </div>
        <hr />
        <div class="container charge-description">
            <p class="label label-info">温馨提示：</p>
            <ol style="margin-top:0.2em;">
                <li>点击获取验证码后，您将收到一条包含验证码的短信，请填写验证码以后完成认证。</li>
                <li>本认证只支持电信手机号码。</li>
            </ol>
        </div>
        <script type="text/javascript">
	    $(document).ready(function() {
		$("#smsBtn").click(function() {
		    var $this = $(this);
		    $.ajax("sms", {
			type: 'POST',
			data: {
			    mobile: $("[name=mobile]").val()
			}
		    }).done(function(txt) {
		    });
		    $this.prop("disabled", true).addClass("disable");
		    var cycle = 5;
		    $this.text(cycle + "秒后可用");
		    var timer = window.setInterval(function() {
			$this.text(--cycle + "秒后可用");
			if (cycle < 0) {
			    window.clearInterval(timer);
			    $this.prop("disabled", false).removeClass("disable");
			    $this.text("获取验证码");
			}
		    }, 1000);
		});
		$("#authcForm").validate({
		    highlight: function(element) {
			$(element).closest('.form-group').addClass('has-error');
		    },
		    unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error');
		    },
		    errorElement: 'span',
		    errorClass: 'help-block',
		    errorPlacement: function(error, element) {
			if (element.parent('.input-group').length) {
			    error.insertAfter(element.parent());
			} else {
			    error.insertAfter(element);
			}
		    }
		});
	    });
        </script>
    </body>
</html>