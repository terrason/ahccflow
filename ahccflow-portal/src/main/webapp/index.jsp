<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width; initial-scale=1.0;minimum-scale=1.0; maximum-scale=2.0;user-scalable=yes" />
        <title><spring:message code="application.title"/></title>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
	<style type="text/css">
	    .jumbotron .btn-lg{margin:0.4em auto;}
	</style>
        <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    </head>
    <body>
	<div class="container">
	    <div class="jumbotron">
		<h1>安徽电信·流量分发平台</h1>
		<p>安徽电信 流量经营互联网运营平台</p>
		<p>
		    <a class="btn btn-info btn-lg" role="button" href="tencent">腾讯入口</a>
		    <a class="btn btn-info btn-lg" role="button" href="baidu">百度入口</a>
		    <a class="btn btn-info btn-lg" role="button" href="zhah">智慧安徽入口</a>
		</p>
	    </div>
	</div>
    </body>
</html>
