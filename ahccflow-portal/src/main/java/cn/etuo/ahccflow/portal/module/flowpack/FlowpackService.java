package cn.etuo.ahccflow.portal.module.flowpack;

import cn.etuo.ahccflow.portal.RepetitiveFloworderException;
import cn.etuo.ahccflow.portal.UnAuthenticationException;
import cn.etuo.ahccflow.portal.UnsupportedChannelException;
import cn.etuo.ahccflow.portal.module.flowpack.response.FlowResponse;
import cn.etuo.ahccflow.portal.module.flowpack.response.SimpleFlowResponse;
import cn.etuo.ahccflow.portal.module.flowpack.vo.Flowpack;
import cn.etuo.ahccflow.portal.util.JsonUtil;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.List;
import java.util.Properties;
import javax.annotation.Resource;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class FlowpackService {

    private final Logger logger = LoggerFactory.getLogger(FlowpackService.class);

    private final MessageFormat ENDPOINT_HOME = new MessageFormat("/{0}/floworder/{1}");
    private final MessageFormat ENDPOINT_ORDERFLOW = new MessageFormat("/{0}/floworder");
    private final MessageFormat ENDPOINT_HOME_EDGED = ENDPOINT_ORDERFLOW;
    private final MessageFormat ENDPOINT_FLOWVIEW = new MessageFormat("/{0}/floworder/{1}/{2}");
    @Value("#{settings['ws.url']}")
    private String url;
    @Resource
    private Properties settings;

    public List<Flowpack> getHomeList(String channel, String ip, String sim) throws UnAuthenticationException, IOException {
	String channelCode = settings.getProperty("channel." + channel);
	if (channelCode == null) {
	    throw new UnsupportedChannelException(channel);
	}
	Document document = Jsoup
		.connect(url + resolveHomeUrl(channelCode, sim))
		.ignoreContentType(true)
		.data("ip", ip)
		.get();
	String body = document.text();
	logger.debug("流量包列表服务返回：{}", body);
	FlowResponse response = JsonUtil.getObjectMapper().readValue(body, FlowResponse.class);
	response.check();
	if (response.getCode() == 401) {
	    throw new UnAuthenticationException(response.getData());
	}
	return response.getData();
    }

    public List<Flowpack> getHomeList(String channel, String ip) throws IOException {
	return getHomeList(channel, ip, null);
    }

    private String resolveHomeUrl(String channel, String key) {
	if (key == null) {
	    return ENDPOINT_HOME_EDGED.format(new Object[]{channel});
	} else {
	    return ENDPOINT_HOME.format(new Object[]{channel, key});
	}
    }

    public Flowpack getFlowpack(String channel, String key, int flowId) throws IOException {
	String channelCode = settings.getProperty("channel." + channel);
	if (channelCode == null) {
	    throw new UnsupportedChannelException(channel);
	}
	Document document = Jsoup
		.connect(url + ENDPOINT_FLOWVIEW.format(new Object[]{channelCode, key, flowId}))
		.ignoreContentType(true)
		.get();
	String body = document.text();
	logger.debug("流量包详细信息服务返回：{}", body);
	SimpleFlowResponse response = JsonUtil.getObjectMapper().readValue(body, SimpleFlowResponse.class);
	response.check();
	return response.getData();
    }

    public void orderFlowpack(String channel, String key, int flowId) throws IOException {
	String channelCode = settings.getProperty("channel." + channel);
	if (channelCode == null) {
	    throw new UnsupportedChannelException(channel);
	}
	Document document = Jsoup
		.connect(url + ENDPOINT_ORDERFLOW.format(new Object[]{channelCode}))
		.ignoreContentType(true)
		.data("key", key)
		.data("flowId", String.valueOf(flowId))
		.post();
	String body = document.text();
	logger.debug("流量包订购服务返回：{}", body);
	SimpleFlowResponse response = JsonUtil.getObjectMapper().readValue(body, SimpleFlowResponse.class);
	if (response.getCode() == -22) {
	    throw new RepetitiveFloworderException(response);
	}
	response.check();
    }
}
