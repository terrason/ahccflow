package cn.etuo.ahccflow.portal;

import cn.etuo.ahccflow.portal.module.flowpack.vo.Flowpack;
import java.util.List;

public class UnAuthenticationException extends RuntimeException {

    private List<Flowpack> dirtyFlows;
    private int flowview;

    public UnAuthenticationException(List<Flowpack> dirtyFlows) {
	this.dirtyFlows = dirtyFlows;
    }

    public UnAuthenticationException(int flowview) {
	this.flowview = flowview;
    }

    public List<Flowpack> getDirtyFlows() {
	return dirtyFlows;
    }

    public void setDirtyFlows(List<Flowpack> dirtyFlows) {
	this.dirtyFlows = dirtyFlows;
    }

    public int getFlowview() {
	return flowview;
    }

    public void setFlowview(int flowview) {
	this.flowview = flowview;
    }

}
