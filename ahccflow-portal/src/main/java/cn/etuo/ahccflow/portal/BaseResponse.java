package cn.etuo.ahccflow.portal;

public class BaseResponse extends AbstractResponse{
	private Object data;

        @Override
	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
