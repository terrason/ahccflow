package cn.etuo.ahccflow.portal.module.flowpack.response;

import cn.etuo.ahccflow.portal.AbstractResponse;
import cn.etuo.ahccflow.portal.module.flowpack.vo.Flowpack;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SimpleFlowResponse extends AbstractResponse {

    private Flowpack data;

    @Override
    public Flowpack getData() {
        return data;
    }

    public void setData(Flowpack data) {
        this.data = data;
    }

}
