/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.etuo.ahccflow.portal.module.flowpack.response;

import cn.etuo.ahccflow.portal.AbstractResponse;
import cn.etuo.ahccflow.portal.module.flowpack.vo.Flowpack;
import java.util.ArrayList;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FlowResponse extends AbstractResponse {

    private ArrayList<Flowpack> data;

    @Override
    public ArrayList<Flowpack> getData() {
	return data;
    }

    public void setData(ArrayList<Flowpack> data) {
	this.data = data;
    }
}
