package cn.etuo.ahccflow.portal.controller;

import cn.etuo.ahccflow.portal.UnAuthenticationException;
import cn.etuo.ahccflow.portal.module.flowpack.FlowpackService;
import cn.etuo.ahccflow.portal.module.flowpack.vo.Flowpack;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {

    @Resource
    private FlowpackService flowpackService;

    @RequestMapping("/{channel}")
    public String home(
	    @PathVariable String channel,
	    @RequestParam(required = false) String key,
	    HttpServletRequest request,
	    HttpSession session,
	    Model model) throws IOException {
	List<Flowpack> flows;
	boolean autherized;
	try {
	    if (StringUtils.hasText(key)) {
		session.setAttribute("key", key);
		autherized = true;
		flows = flowpackService.getHomeList(channel, getPrincipalIp(request), key);
	    } else {
		key = (String) session.getAttribute("key");
		if (key != null) {
		    autherized = true;
		    flows = flowpackService.getHomeList(channel, getPrincipalIp(request), key);
		} else {
		    autherized = false;
		    flows = flowpackService.getHomeList(channel, getPrincipalIp(request));
		}
	    }
	} catch (UnAuthenticationException ex) {
	    flows = ex.getDirtyFlows();
	    autherized = false;
	}
	session.setAttribute("autherized", autherized);
	model.addAttribute("channel", channel);
	exposeToModel(flows, model);
	return "home";
    }

    private String getPrincipalIp(HttpServletRequest req) {
	String lastLoginIP = req.getHeader("x-forwarded-for");
	if (lastLoginIP == null || lastLoginIP.length() == 0 || "unknown".equalsIgnoreCase(lastLoginIP)) {
	    lastLoginIP = req.getHeader("Proxy-Client-IP");
	}
	if (lastLoginIP == null || lastLoginIP.length() == 0 || "unknown".equalsIgnoreCase(lastLoginIP)) {
	    lastLoginIP = req.getHeader("WL-Proxy-Client-IP");
	}
	if (lastLoginIP == null || lastLoginIP.length() == 0 || "unknown".equalsIgnoreCase(lastLoginIP)) {
	    lastLoginIP = req.getRemoteAddr();
	}
	return lastLoginIP;
    }

    private void exposeToModel(List<Flowpack> flows, Model model) {
	List<Flowpack> flowList = new ArrayList<Flowpack>();
	List<Flowpack> additionalFlowList = new ArrayList<Flowpack>();
	List<Flowpack> easyFlowList = new ArrayList<Flowpack>();
	for (int i = 0; i < flows.size(); i++) {
	    Flowpack f = flows.get(i);
	    switch (f.getCatalog()) {
		case 1:
		    flowList.add(f);
		    break;
		case 2:
		    additionalFlowList.add(f);
		    break;
		case 3:
		    easyFlowList.add(f);
		    break;
	    }
	}
	model.addAttribute("flowList", flowList);
	model.addAttribute("additionalFlowList", additionalFlowList);
	model.addAttribute("easyFlowList", easyFlowList);
    }

    @RequestMapping(value = "/{channel}/{flowId}", method = RequestMethod.GET)
    public String view(@PathVariable String channel, @PathVariable int flowId, HttpSession session, Model model) throws IOException {
	Boolean autherized = (Boolean) session.getAttribute("autherized");
	if (autherized == null) {
	    return "redirect:/" + channel;
	} else if (!autherized) {
	    throw new UnAuthenticationException(flowId);
	}
	String key = (String) session.getAttribute("key");
	if (!StringUtils.hasText(key)) {
	    throw new UnAuthenticationException(flowId);
	}

	Flowpack flowpack = flowpackService.getFlowpack(channel, key, flowId);
	model.addAttribute("flow", flowpack);

	model.addAttribute("channel", channel);
	return "info";
    }

    @RequestMapping(value = "/{channel}/floworder", method = RequestMethod.POST)
    public String orderFlow(@PathVariable String channel, @RequestParam int id, HttpSession session, Model model) throws IOException {
	Boolean autherized = (Boolean) session.getAttribute("autherized");
	if (autherized == null) {
	    return "redirect:/" + channel;
	} else if (!autherized) {
	    throw new UnAuthenticationException(id);
	}
	String key = (String) session.getAttribute("key");
	if (!StringUtils.hasText(key)) {
	    throw new UnAuthenticationException(id);
	}

	flowpackService.orderFlowpack(channel, key, id);

	model.addAttribute("channel", channel);
	return "success";
    }
}
