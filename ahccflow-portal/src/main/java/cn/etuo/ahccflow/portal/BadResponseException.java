package cn.etuo.ahccflow.portal;

public class BadResponseException extends RuntimeException {
    private int code;

    public BadResponseException(AbstractResponse response) {
	super(response.getDescription());
	code=response.getCode();
    }

    public BadResponseException(int code, String message) {
	super(message);
	this.code=code;
    }

    public BadResponseException(int code, String message, Throwable cause) {
	super(message, cause);
	this.code=code;
    }

    public int getCode() {
	return code;
    }

    public void setCode(int code) {
	this.code = code;
    }
}
