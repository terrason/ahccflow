package cn.etuo.ahccflow.portal.module.flowpack;

import cn.etuo.ahccflow.portal.BaseResponse;
import cn.etuo.ahccflow.portal.util.JsonUtil;
import java.io.IOException;
import java.text.MessageFormat;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class AuthenticationService {

    private final Logger logger = LoggerFactory.getLogger(AuthenticationService.class);
    private final MessageFormat ENDPOINT_SMS = new MessageFormat("/smscode/{0}");
    private final String ENDPOINT_REGIST = "/regist";
    @Value("#{settings['sms.url']}")
    private String smsUrl;
    @Value("#{settings['ws.url']}")
    private String wsUrl;

    public String sendSms(String mobile) throws IOException {
	Document document = Jsoup
		.connect(smsUrl + ENDPOINT_SMS.format(new Object[]{mobile}))
		.ignoreContentType(true)
		.get();
	String body = document.text();
	logger.debug("发送短信验证码服务返回：{}", body);
	BaseResponse response = JsonUtil.getObjectMapper().readValue(body, BaseResponse.class);
	response.check();
	return response.getData().toString();
    }

    public String signIn(String mobile, String sim) throws IOException {
	Connection connection = Jsoup.connect(wsUrl + ENDPOINT_REGIST)
		.ignoreContentType(true)
		.data("mobile", mobile);
	if (StringUtils.hasText(sim)) {
	    connection.data("key", sim);
	}
	Document document = connection.post();
	String body = document.text();
	logger.debug("登录认证服务返回：{}", body);
	BaseResponse response = JsonUtil.getObjectMapper().readValue(body, BaseResponse.class);
	response.check();
	return response.getData().toString();
    }
}
