package cn.etuo.ahccflow.portal.controller;

import cn.etuo.ahccflow.portal.BadResponseException;
import cn.etuo.ahccflow.portal.RepetitiveFloworderException;
import cn.etuo.ahccflow.portal.UnAuthenticationException;
import cn.etuo.ahccflow.portal.UnsupportedChannelException;
import javax.servlet.http.HttpServletRequest;
import org.jsoup.HttpStatusException;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class GlobalExceptionHandler {

    public static final String DEFAULT_ERROR_VIEW = "error";

    @ExceptionHandler
    public ModelAndView defaultErrorHandler(HttpServletRequest req, Exception ex) throws Exception {
	ModelAndView mav = createModelAndView(req, ex);
	mav.setViewName(DEFAULT_ERROR_VIEW);
	return mav;
    }

    @ResponseStatus(value = HttpStatus.NOT_IMPLEMENTED)
    @ExceptionHandler(value = HttpStatusException.class)
    public ModelAndView unsupportedChannelExceptionHandler(HttpServletRequest req, HttpStatusException ex) throws Exception {
	ModelAndView mav = createModelAndView(req, ex);
	mav.addObject("code", ex.getStatusCode());
	mav.addObject("url", ex.getUrl());
	mav.setViewName(DEFAULT_ERROR_VIEW);
	return mav;
    }
    @ResponseStatus(value = HttpStatus.NOT_IMPLEMENTED)
    @ExceptionHandler(value = UnsupportedChannelException.class)
    public ModelAndView unsupportedChannelExceptionHandler(HttpServletRequest req, UnsupportedChannelException ex) throws Exception {
	ModelAndView mav = createModelAndView(req, ex);
	mav.addObject("code", HttpStatus.NOT_IMPLEMENTED.value());
	mav.setViewName(DEFAULT_ERROR_VIEW);
	return mav;
    }

    @ExceptionHandler(value = UnAuthenticationException.class)
    public ModelAndView authenticationExceptionHandler(HttpServletRequest req, UnAuthenticationException ex) throws Exception {
	ModelAndView mav = createModelAndView(req, ex);
	mav.setViewName("authc");
	return mav;
    }

    @ExceptionHandler(value = RepetitiveFloworderException.class)
    public ModelAndView authenticationExceptionHandler(HttpServletRequest req, RepetitiveFloworderException ex) throws Exception {
	ModelAndView mav = createModelAndView(req, ex);
	mav.setViewName("failure");
	return mav;
    }

    private ModelAndView createModelAndView(HttpServletRequest req, Exception ex) throws Exception {
	// If the exception is annotated with @ResponseStatus rethrow it and let
	// the framework handle it.
	// AnnotationUtils is a Spring Framework utility class.
	if (AnnotationUtils.findAnnotation(ex.getClass(), ResponseStatus.class) != null) {
	    throw ex;
	}

	// Otherwise setup and send the user to a default error-view.
	ModelAndView mav = new ModelAndView();
	mav.addObject("exception", ex);
	mav.addObject("url", req.getRequestURL());
	if (ex instanceof BadResponseException) {
	    mav.addObject("code", ((BadResponseException) ex).getCode());
	}
	return mav;
    }
}
