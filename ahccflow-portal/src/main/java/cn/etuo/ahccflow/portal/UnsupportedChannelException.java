package cn.etuo.ahccflow.portal;

public class UnsupportedChannelException extends RuntimeException {

    public UnsupportedChannelException(String channel) {
	super("尚未开通的渠道：" + channel);
    }

}
