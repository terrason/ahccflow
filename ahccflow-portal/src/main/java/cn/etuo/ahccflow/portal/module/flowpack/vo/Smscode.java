/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.etuo.ahccflow.portal.module.flowpack.vo;

import java.util.Calendar;
import java.util.Date;

public class Smscode{

    private String code;
    private Date ts;

    public Smscode(String code) {
        if(code==null || code.length()!=6){
            throw new RuntimeException("不识别的短信验证码："+code);
        }
        this.code = code;
        ts=new Date();
    }

    public String getCode() {
        return code;
    }

    public boolean isExpired() {
        Calendar now=Calendar.getInstance();
        Calendar c=Calendar.getInstance();
        c.setTime(ts);
        c.add(Calendar.MINUTE, 5);
        return c.before(now);
    }
}
