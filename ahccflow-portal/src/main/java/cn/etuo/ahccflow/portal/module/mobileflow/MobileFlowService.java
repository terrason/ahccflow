package cn.etuo.ahccflow.portal.module.mobileflow;

import cn.etuo.ahccflow.portal.module.mobileflow.response.IFQryFlowDto;
import java.io.IOException;
import org.codehaus.jackson.map.ObjectMapper;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class MobileFlowService {

    private final Logger logger = LoggerFactory.getLogger(MobileFlowService.class);
    private final String ENDPOINT_FLOWQUERY = "/flowQuery";

    @Value("#{settings['ws.url']}")
    private String url;
    private final ObjectMapper json = new ObjectMapper();

    public IFQryFlowDto queryMobileFlowInfo(String sim) throws IOException {
	String fetchUrl=url + ENDPOINT_FLOWQUERY;
	logger.debug("查询手机流量信息： post:{}",fetchUrl);
	Connection connection = Jsoup.connect(fetchUrl)
		.ignoreContentType(true);
	if (StringUtils.hasText(sim)) {
	    logger.debug("sim卡号：{}",sim);
	    connection.data("key", sim);
	}
	Document document = connection.post();
	String body = document.text();
	logger.debug("手机流量查询服务返回：{}", body);
	return json.readValue(body, IFQryFlowDto.class);
    }
}
