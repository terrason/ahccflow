package cn.etuo.ahccflow.portal.controller;

import cn.etuo.ahccflow.portal.AbstractResponse;
import cn.etuo.ahccflow.portal.BaseResponse;
import cn.etuo.ahccflow.portal.module.flowpack.AuthenticationService;
import cn.etuo.ahccflow.portal.module.flowpack.vo.Smscode;
import java.io.IOException;
import java.util.HashMap;
import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AuthenticationController {

    private final Logger logger = LoggerFactory.getLogger(AuthenticationController.class);
    @Resource
    private AuthenticationService authenticationService;

    @RequestMapping(value = "/{channel}/sms", method = RequestMethod.POST)
    @ResponseBody
    public AbstractResponse sms(@RequestParam String mobile, HttpSession session) throws IOException {
	String smscode = authenticationService.sendSms(mobile);
	putSessionSmscode(session, smscode);
	logger.debug("发送短信验证码：{}", smscode);
	return new BaseResponse();
    }

    @RequestMapping(value = "/{channel}/signIn", method = RequestMethod.POST)
    public String signIn(@PathVariable String channel,
	    @RequestParam String mobile,
	    @RequestParam String smsAuthenticationCode,
	    @RequestParam(required = false) String flowview,
	    HttpSession session,
	    Model model) throws IOException {
	if (!StringUtils.hasText(smsAuthenticationCode)) {
	    throw new IllegalArgumentException("验证码不能为空");
	}
	if (!isSmscodeActive(session, smsAuthenticationCode)) {
	    throw new IllegalArgumentException("验证码不正确");
	}
	invalidateSmscodes(session);
	if (!StringUtils.hasText(mobile)) {
	    throw new IllegalArgumentException("手机号码不能为空");
	}
	String key = authenticationService.signIn(mobile, (String) session.getAttribute("key"));
	session.setAttribute("key", key);
	session.setAttribute("autherized", true);

	//跳转到登录前页面
	StringBuilder redirectUrl = new StringBuilder();
	redirectUrl.append("redirect:/").append(channel);
	if (StringUtils.hasText(flowview)) {
	    redirectUrl.append("/").append(flowview);
	}
	return redirectUrl.toString();
    }

    private boolean isSmscodeActive(HttpSession session, String smscode) {
	HashMap<String, Smscode> smscodes = (HashMap<String, Smscode>) session.getAttribute("smscodes");
	if (smscodes == null) {
	    return false;
	}
	Smscode scode = smscodes.get(smscode);
	if (scode == null) {
	    return false;
	}
	if (scode.isExpired()) {
	    smscodes.remove(smscode);
	    return false;
	}
	return true;
    }

    private void invalidateSmscodes(HttpSession session) {
	session.removeAttribute("smscodes");
    }

    private void putSessionSmscode(HttpSession session, String smscode) {
	HashMap<String, Smscode> smscodes = (HashMap<String, Smscode>) session.getAttribute("smscodes");
	if (smscodes == null) {
	    smscodes = new HashMap<String, Smscode>();
	    session.setAttribute("smscodes", smscodes);
	}
	smscodes.put(smscode, new Smscode(smscode));
    }
}
