package cn.etuo.ahccflow.portal.controller;

import cn.etuo.ahccflow.portal.module.mobileflow.MobileFlowService;
import cn.etuo.ahccflow.portal.module.mobileflow.response.IFQryFlowDto;
import java.io.IOException;
import javax.annotation.Resource;
import org.jsoup.HttpStatusException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class EdgedQueryMobileFlowInfoController {

    private final Logger logger = LoggerFactory.getLogger(EdgedQueryMobileFlowInfoController.class);
    @Resource
    private MobileFlowService mobileFlowService;

    @RequestMapping("/flowQuery")
    @ResponseBody
    public IFQryFlowDto execute(@RequestParam(defaultValue = " > < ||") String key) {
	try {
	    return mobileFlowService.queryMobileFlowInfo(key);
	} catch (HttpStatusException ex) {
	    logger.warn("fetch http error:[" + ex.getStatusCode() + "] " + ex.getUrl(), ex);
	    IFQryFlowDto result = new IFQryFlowDto();
	    result.setCode(ex.getStatusCode());
	    result.setDesc("网络异常");
	    return result;
	} catch (IOException ex) {
	    logger.warn("向服务器查询流量接口时出现网络错误", ex);
	    IFQryFlowDto result = new IFQryFlowDto();
	    result.setCode(1);
	    result.setDesc("网络异常");
	    return result;
	}
    }
}
