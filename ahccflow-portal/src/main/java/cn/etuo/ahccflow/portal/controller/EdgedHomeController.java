package cn.etuo.ahccflow.portal.controller;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class EdgedHomeController {

    @RequestMapping("/flowordering/getList")
    public String home(
	    @RequestParam(required = false) String key,
	    HttpServletRequest request,
	    HttpSession session,
	    Model model) throws IOException {
	if (StringUtils.hasText(key)) {
	    session.setAttribute("key", key);
	}
	return "redirect:/tencent";
    }
}
