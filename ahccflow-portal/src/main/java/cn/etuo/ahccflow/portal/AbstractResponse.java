package cn.etuo.ahccflow.portal;

public abstract class AbstractResponse {

    private int code;
    private String description;

    public int getCode() {
	return code;
    }

    public void setCode(int code) {
	this.code = code;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public abstract Object getData();

    @Override
    public String toString() {
	return "AbstractResponse{" + "code=" + code + ", description=" + description + '}';
    }

    public void check() throws BadResponseException {
	if (code < 0) {
	    throw new BadResponseException(code, description);
	}
    }
}
