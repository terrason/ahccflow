package cn.etuo.ahccflow.portal.module.mobileflow.response;

public class IFQryFlowDto implements java.io.Serializable {

    private int code;//0-查询成功  1-查询失败
    private double total;//总流量
    private double used;//已使用流量
    private double leave;//剩余流量
    private double over;//超出流量
    private String desc;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getUsed() {
        return used;
    }

    public void setUsed(double used) {
        this.used = used;
    }

    public double getLeave() {
        return leave;
    }

    public void setLeave(double leave) {
        this.leave = leave;
    }

    public double getOver() {
        return over;
    }

    public void setOver(double over) {
        this.over = over;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "IFQryFlowDto{" + "code=" + code + ", total=" + total + ", used=" + used + ", leave=" + leave + ", over=" + over + ", desc=" + desc + '}';
    }
}
